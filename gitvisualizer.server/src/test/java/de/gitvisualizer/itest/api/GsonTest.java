package de.gitvisualizer.itest.api;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;

import de.gitvisualizer.api.repository.BranchItem;
import de.gitvisualizer.api.repository.BranchItemConnection;
import de.gitvisualizer.api.repository.CommitDescription;
import de.gitvisualizer.api.repository.LogicalBranch;
import de.gitvisualizer.api.repository.LogicalRepository;
import de.gitvisualizer.api.repository.items.BranchStart;
import de.gitvisualizer.api.repository.items.SingleCommit;

public class GsonTest {

  private int idCounter = 0;

  @Test
  public void toGson() throws Exception {

    // master: X-Y-Z---------H>ZZ
    // release: C>D-F-H
    // develop: A-[B1,B2,B3]-C

    BranchStart masterBranchStart = new BranchStart(newId(), "master");
    BranchStart developBranchStart = new BranchStart(newId(), "develop");

    SingleCommit v1Commit = newSingleCommit();
    SingleCommit v2Commit = newSingleCommit();
    SingleCommit v3Commit = newSingleCommit();

    LogicalBranch masterBranch = new LogicalBranch("master", asList(masterBranchStart, v1Commit, v2Commit, v3Commit));
    LogicalBranch developBranch = new LogicalBranch("develop", asList(developBranchStart, newSingleCommit(),
        newSingleCommit(), newSingleCommit()));

    BranchStart releaseBranchStart = new BranchStart(newId(), "release/v4");
    SingleCommit releaseCommit = newSingleCommit();
    LogicalBranch releaseBranch = new LogicalBranch("release/v4", asList(releaseBranchStart, releaseCommit));

    BranchItemConnection connection = new BranchItemConnection("merge", v3Commit.getId(), releaseCommit.getId());

    LogicalRepository repository = new LogicalRepository(newBranches(masterBranch, developBranch, releaseBranch),
        newConnections(connection));

    Gson gson = new Gson();
    String b = gson.toJson(repository);
    System.out.println(b);

  }

  private List<BranchItemConnection> newConnections(BranchItemConnection... connections) {
    List<BranchItemConnection> result = new LinkedList<BranchItemConnection>();

    for (BranchItemConnection branchItemConnection : connections) {
      result.add(branchItemConnection);
    }

    return result;
  }

  private List<LogicalBranch> newBranches(LogicalBranch... branches) {
    List<LogicalBranch> result = new LinkedList<LogicalBranch>();

    for (LogicalBranch logicalBranch : branches) {
      result.add(logicalBranch);
    }

    return result;
  }

  private List<BranchItem> asList(BranchItem... items) {
    List<BranchItem> result = new LinkedList<BranchItem>();

    for (BranchItem branchItem : items) {
      result.add(branchItem);
    }

    return result;
  }

  private SingleCommit newSingleCommit() {
    String sha = "commit-" + newId();
    return new SingleCommit(newId(), newCommitDescription(sha), Collections.emptyList());
  }

  private CommitDescription newCommitDescription(final String sha) {
    CommitDescription commitDescription = new CommitDescription(sha, "Commit " + sha, "Commit " + sha
        + "\n\nA very long commit message", Collections.emptyList());
    return commitDescription;
  }

  private String newId() {
    return "item-" + idCounter++;
  }

}
