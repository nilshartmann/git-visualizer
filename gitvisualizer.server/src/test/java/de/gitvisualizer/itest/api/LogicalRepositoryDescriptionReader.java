package de.gitvisualizer.itest.api;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.gitvisualizer.api.repository.BranchItem;
import de.gitvisualizer.api.repository.BranchItemConnection;
import de.gitvisualizer.api.repository.CommitDescription;
import de.gitvisualizer.api.repository.LogicalBranch;
import de.gitvisualizer.api.repository.LogicalRepository;
import de.gitvisualizer.api.repository.items.BranchStart;
import de.gitvisualizer.api.repository.items.CombinedCommit;
import de.gitvisualizer.api.repository.items.SingleCommit;

public class LogicalRepositoryDescriptionReader {
  private final File       file;

  private final LoremIpsum loremIpsum            = new LoremIpsum();

  private final int        minCommitMessageWords = 2;

  private final int        maxCommitMessageWords = 5;

  private int              commitMessageWords    = minCommitMessageWords;

  protected LogicalRepositoryDescriptionReader(File file) {
    this.file = file;
  }

  public static void main(String[] args) throws Exception {
    //
    String mockDir = "/Users/nils/develop/wjax2014_git_workshop/git-visualizer/gitvisualizer.server/repository-mocks";
    File inDir = new File(mockDir, "in");
    // File outDir = new File(mockDir, "out");
    File outDir = new File(
        "/Users/nils/develop/wjax2014_git_workshop/git-visualizer/gitvisualizer.client/test/fixtures");
    if (!outDir.exists()) {
      outDir.mkdirs();
    }

    File[] inFiles = inDir.listFiles(new FileFilter() {
      @Override
      public boolean accept(File pathname) {
        return pathname.isFile() && pathname.getName().endsWith(".txt");
      }
    });

    for (File file : inFiles) {
      System.out.println("PARSE: " + file);
      LogicalRepository repository = LogicalRepositoryDescriptionReader.parseDescription(file);

      System.out.print(repository.toShortString());
      String name = file.getName();
      name = name.substring(0, name.length() - ".txt".length());
      name = name + ".json";
      File outFile = new File(outDir, name);
      System.out.println(" ==> JSON: " + outFile);

      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      FileWriter fileWriter = new FileWriter(outFile);
      gson.toJson(repository, fileWriter);
      fileWriter.close();
      System.out.println("---------------------------------------------------------------------------------");

    }

    // LogicalRepository repository = LogicalRepositoryDescriptionReader
    // .parseDescription("/Users/nils/develop/wjax2014_git_workshop/git-visualizer/gitvisualizer.server/network.txt");
    //
    // System.out.println(repository.toShortString());
    //
  }

  public static LogicalRepository parseDescription(String fileName) throws Exception {

    File file = new File(fileName);
    return parseDescription(file);

  }

  public static LogicalRepository parseDescription(File file) throws Exception {
    assertTrue(file.exists());

    LogicalRepositoryDescriptionReader reader = new LogicalRepositoryDescriptionReader(file);
    return reader.parse();
  }

  protected LogicalRepository parse() throws Exception {
    List<String> lines = Files.readLines(file, Charsets.UTF_8);

    final List<LogicalBranch> logicalBranches = new LinkedList<LogicalBranch>();
    final List<BranchItemConnection> branchItemConnections = new LinkedList<BranchItemConnection>();

    for (String line : lines) {
      line = line.trim();
      if (line.isEmpty() || line.startsWith("#")) {
        continue;
      }

      if (line.startsWith("connect")) {
        String definition = line.substring("connect".length()).trim();
        List<BranchItemConnection> connections = parseConnection(definition);
        branchItemConnections.addAll(connections);

        continue;
      }

      int j = line.indexOf(':');
      if (j < 1) {
        throw new IllegalStateException("Invalid line " + line);
      }
      String branchName = line.substring(0, j).trim();
      String definition = line.substring(j).trim();

      List<BranchItem> branchItems = parseBranch(branchName, definition);

      LogicalBranch logicalBranch = new LogicalBranch(branchName, branchItems);

      logicalBranches.add(logicalBranch);

    }

    LogicalRepository logicalRepository = new LogicalRepository(logicalBranches, branchItemConnections);
    return logicalRepository;
  }

  private final Map<String, BranchItem> allBranchItems = new Hashtable<>();

  private void assertItemExists(String s) {
    if (!allBranchItems.containsKey(s)) {
      throw new IllegalStateException("No Item '" + s + "'");
    }
  }

  private void registerItem(BranchItem bi) {
    if (allBranchItems.containsKey(bi.getId())) {
      throw new IllegalStateException("Item already registered: '" + bi.getId() + "'");
    }

    allBranchItems.put(bi.getId(), bi);
  }

  private List<BranchItemConnection> parseConnection(String definition) {
    final List<BranchItemConnection> result = new LinkedList<BranchItemConnection>();

    String[] definitions = definition.split(",");
    for (String string : definitions) {
      int j = string.indexOf("-");
      String from = string.substring(0, j).trim();
      String to = string.substring(j + 1).trim();
      assertItemExists(from);
      assertItemExists(to);

      String connectionType = "merge";
      if (allBranchItems.get(to) instanceof BranchStart) {
        connectionType = "branchStart";
      }

      BranchItemConnection connection = new BranchItemConnection(connectionType, from, to);
      result.add(connection);
    }

    return result;

  }

  private final Map<String, AtomicInteger> idCache = new Hashtable<String, AtomicInteger>();

  private String newId(String p) {
    if (!idCache.containsKey(p)) {
      idCache.put(p, new AtomicInteger(0));
    }

    return p + "-" + idCache.get(p).incrementAndGet();

  }

  enum TokenType {
    BRANCH_START, COMMIT, COMBINED_COMMIT
  }

  private List<BranchItem> parseBranch(String branchName, String definition) throws Exception {

    final List<BranchItem> result = new LinkedList<BranchItem>();

    TokenType currentToken = null;
    StringBuilder currentTokenString = null;

    for (char c : definition.toCharArray()) {

      if (c == '!') {
        currentToken = TokenType.BRANCH_START;
        // Neuer Branch Start
        continue;
      } else if (Character.isLetterOrDigit(c)) {
        if (currentTokenString == null) {
          currentTokenString = new StringBuilder();
        }
        currentTokenString.append(c);
      } else if (c == '[') {
        currentToken = TokenType.COMBINED_COMMIT;
        currentTokenString = new StringBuilder();
      } else if (c == ']') {
        String ts = currentTokenString.toString();
        int j = ts.indexOf(':');
        String id = null;
        if (j > 0) {
          id = ts.substring(0, j);
          ts = ts.substring(j + 1);
        }

        if (id == null) {
          id = newId("cc");
        }
        String[] commitIds = ts.split(",");
        List<CommitDescription> commitDescriptions = new LinkedList<CommitDescription>();
        for (String cid : commitIds) {
          cid = cid.trim();
          commitDescriptions.add(newCommitDescription(cid));
        }

        CombinedCommit combinedCommit = new CombinedCommit(id, commitDescriptions);
        registerItem(combinedCommit);
        result.add(combinedCommit);

        currentToken = null;
        currentTokenString = null;
      } else {
        if (currentToken == TokenType.COMBINED_COMMIT) {
          currentTokenString.append(c);
          continue;
        }
        if (currentToken == TokenType.BRANCH_START) {

          String id = null;
          if (currentTokenString != null && currentTokenString.length() > 0) {
            id = currentTokenString.toString().trim();
          } else {
            id = newId("branch-start");
          }

          BranchStart branchStart = new BranchStart(id, branchName);
          registerItem(branchStart);
          result.add(branchStart);
          currentToken = null;
          currentTokenString = null;
          continue;
        }

        if (currentTokenString != null) {
          SingleCommit singleCommit = new SingleCommit(currentTokenString.toString(),
              newCommitDescription(currentTokenString.toString()), Collections.emptyList());
          result.add(singleCommit);
          registerItem(singleCommit);
          currentTokenString = null;
        }
      }
    }

    if (currentTokenString != null) {
      SingleCommit singleCommit = new SingleCommit(currentTokenString.toString(),
          newCommitDescription(currentTokenString.toString()), Collections.emptyList());
      result.add(singleCommit);
      registerItem(singleCommit);
      currentTokenString = null;
    }
    return result;

  }

  private CommitDescription newCommitDescription(final String sha) {
    StringBuilder commitMessageBuilder = new StringBuilder();
    String shortMessage = loremIpsum.getWords(commitMessageWords++);
    if (commitMessageWords > maxCommitMessageWords) {
      commitMessageWords = minCommitMessageWords;
    }
    commitMessageBuilder.append(shortMessage).append("\n\n");
    commitMessageBuilder.append(loremIpsum.getParagraphs(1));
    CommitDescription commitDescription = new CommitDescription(sha, shortMessage, commitMessageBuilder.toString(),
        Collections.emptyList());
    return commitDescription;
  }
}
