package de.gitvisualizer.server;

import org.junit.Assert;
import org.junit.Test;

public class BranchesTest {
  @Test
  public void testShortName() {
    Assert.assertEquals("master", Branches.shortName("master"));
    Assert.assertEquals("master", Branches.shortName("refs/heads/master"));
    Assert.assertEquals("master", Branches.shortName("refs/remotes/origin/master"));
    Assert.assertEquals("f/master", Branches.shortName("f/master"));
    Assert.assertEquals("f/master", Branches.shortName("refs/heads/f/master"));
    Assert.assertEquals("f/master", Branches.shortName("refs/remotes/origin/f/master"));
  }

  @Test(expected = IllegalStateException.class)
  public void testShortNameMissingRemote() {
    Assert.assertEquals("master", Branches.shortName("refs/remotes/master"));
  }

}
