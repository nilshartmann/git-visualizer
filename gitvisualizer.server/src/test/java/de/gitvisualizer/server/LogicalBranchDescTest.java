package de.gitvisualizer.server;

import org.junit.Assert;
import org.junit.Test;

import de.gitvisualizer.api.LogicalBranchConf;

public class LogicalBranchDescTest {

  @Test
  public void testInitWithPostfixPattern() {
    LogicalBranchDesc desc = new LogicalBranchDesc(new LogicalBranchConf("release:release/.+"));
    Assert.assertEquals("release", desc.getName());
    Assert.assertEquals("release/.+", desc.getBranchPatterns().get(0).pattern());
    Assert.assertTrue(desc.matches("release/rel"));
    Assert.assertFalse(desc.matches("release"));
    Assert.assertFalse(desc.matches("release/"));
    Assert.assertFalse(desc.matches("relea/rel"));

  }
}
