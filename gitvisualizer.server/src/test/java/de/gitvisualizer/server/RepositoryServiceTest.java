package de.gitvisualizer.server;


import de.gitvisualizer.api.RepositoryAnalyzeConf;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;

public class RepositoryServiceTest {

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void testFindSingleRepo() throws IOException, GitAPIException {
    File newFolder = folder.newFolder("first");
    Git.init().setBare(true).setDirectory(newFolder).call();

    RepositoryAnalyzeConfHandler confHandler = Mockito.mock(RepositoryAnalyzeConfHandler.class);
    Mockito.when(confHandler.readFromRepo(Mockito.any())).thenReturn(new RepositoryAnalyzeConf());

    RepositoryService repositoryService = new RepositoryService(folder.getRoot(),confHandler);
    Assert.assertTrue(newFolder.exists());
    Assert.assertNotNull(repositoryService.findRepo("first"));
  }

  @Test
  public void testRemoveRepoWithoutConfig() throws IOException, GitAPIException {
    File newFolder = folder.newFolder("first");
    Git.init().setBare(true).setDirectory(newFolder).call();

    RepositoryAnalyzeConfHandler confHandler = Mockito.mock(RepositoryAnalyzeConfHandler.class);
    Mockito.when(confHandler.readFromRepo(Mockito.any())).thenReturn(null);

    RepositoryService repositoryService = new RepositoryService(folder.getRoot(),confHandler);
    Assert.assertFalse(newFolder.exists());
  }
}
