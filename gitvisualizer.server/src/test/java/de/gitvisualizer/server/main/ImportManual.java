package de.gitvisualizer.server.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.gitvisualizer.api.repository.LogicalRepository;
import de.gitvisualizer.server.GitRepository;
import de.gitvisualizer.server.LogicalRepositoryAnalyzer;
import de.gitvisualizer.server.RepositoryService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.File;
import java.io.IOException;

@Configuration
@PropertySource("classpath:application.properties")
public class ImportManual {


  public static void main(String[] args) throws IOException {
    try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Application.class,
                                                                                            ImportManual.class)) {
      RepositoryService repositoryService = context.getBean(RepositoryService.class);
      GitRepository repo = repositoryService.findRepo(args[0]);
      LogicalRepositoryAnalyzer repositoryAnalyzer=new LogicalRepositoryAnalyzer(repo);
      LogicalRepository logicalRepository = repositoryAnalyzer.analyze();

      ObjectMapper objectMapper = context.getBean(ObjectMapper.class);
      objectMapper.writerWithType(LogicalRepository.class).writeValue(new File(repo.getDir(),"result.json"),logicalRepository);

    }

  }
}
