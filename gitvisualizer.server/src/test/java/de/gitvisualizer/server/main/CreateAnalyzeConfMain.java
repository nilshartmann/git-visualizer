package de.gitvisualizer.server.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.gitvisualizer.api.LogicalBranchConf;
import de.gitvisualizer.api.MergeCommitPattern;
import de.gitvisualizer.api.RepositoryAnalyzeConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;


@Configuration
@EnableAutoConfiguration
public class CreateAnalyzeConfMain {

    public static void main(String[] args) throws IOException {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(CreateAnalyzeConfMain.class)) {
            ObjectMapper objectMapper = context.getBean(ObjectMapper.class);
            RepositoryAnalyzeConf analyzeConf = new RepositoryAnalyzeConf(
                    Arrays.asList(
                            new LogicalBranchConf("bugfix-rc", "bugfix-rc",  3),
                            new LogicalBranchConf("bugfix", "bugfix", 4),
                            new LogicalBranchConf("prod", "prod",  10),
                            new LogicalBranchConf("int", "int",  0),
                            new LogicalBranchConf("master", "master", 5)),
                    Arrays.asList(
                            new MergeCommitPattern("Automatischer Merge vom Branch '(\\S+)' auf '(\\S+)'",1,2),
                            new MergeCommitPattern("Automatischer Merge von Branch '(\\S+)' auf '(\\S+)'",1,2)));


            File analyzeConfFile = new File(new File("repos"), "analyze.conf.json");
            try {
                objectMapper.writerWithType(RepositoryAnalyzeConf.class).writeValue(analyzeConfFile, analyzeConf);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
