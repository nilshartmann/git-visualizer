package de.gitvisualizer.server;

import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.junit.RepositoryTestCase;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NoteMapCacheTest extends RepositoryTestCase {
  private Git git;

  private RevCommit firstCommitId;

  private RevCommit secondCommitId;
  private RevWalk   revWalk;

  @Before
  public void initGit() throws IOException, GitAPIException {
    git = new Git(db);
    revWalk = new RevWalk(git.getRepository());

    firstCommitId = commitFile("common1.txt", "common", "master");
    secondCommitId = commitFile("common2.txt", "common", "master");
  }

  @Test
  public void testEmptyNotes() {
    NoteMapCache cache = new NoteMapCache(git, revWalk);
    BranchNotes note1 = cache.getNote(firstCommitId);
    Assert.assertTrue(note1.isEmpty());

    BranchNotes note2 = cache.getNote(secondCommitId);
    Assert.assertTrue(note2.isEmpty());

  }

  @Test
  public void testWriteReadNotes() {
    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      BranchNotes note1 = cache.getNote(firstCommitId);
      note1.setProperty("p1", "v1");
      note1.setProperty("p2", "v2");

      BranchNotes note2 = cache.getNote(secondCommitId);
      note2.setProperty("p3", "v3");
      note2.setProperty("p4", "v4");

      boolean changes = cache.writeNotes("My Note");
      Assert.assertTrue(changes);
    }

    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      BranchNotes note1 = cache.getNote(firstCommitId);
      Assert.assertEquals(2, note1.size());
      Assert.assertEquals("v1", note1.getProperty("p1"));
      Assert.assertEquals("v2", note1.getProperty("p2"));

      BranchNotes note2 = cache.getNote(secondCommitId);
      Assert.assertEquals(2, note2.size());
      Assert.assertEquals("v3", note2.getProperty("p3"));
      Assert.assertEquals("v4", note2.getProperty("p4"));
    }
  }

  @Test
  public void testRemoveReadNotes() {
    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      BranchNotes note1 = cache.getNote(firstCommitId);
      note1.setProperty("p1", "v1");
      note1.setProperty("p2", "v2");

      boolean changes = cache.writeNotes("My Note");
      Assert.assertTrue(changes);
    }

    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      BranchNotes note1 = cache.getNote(firstCommitId);
      note1.clear();
      boolean changes = cache.writeNotes("My Note");
      Assert.assertTrue(changes);
    }

    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      BranchNotes note1 = cache.getNote(firstCommitId);
      Assert.assertTrue(note1.isEmpty());
    }
  }

  @Test
  public void testRemoveAllNotes() {
    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      BranchNotes note1 = cache.getNote(firstCommitId);
      note1.setProperty("p1", "v1");
      note1.setProperty("p2", "v2");

      BranchNotes note2 = cache.getNote(secondCommitId);
      note2.setProperty("p3", "v3");
      note2.setProperty("p4", "v4");

      boolean changes = cache.writeNotes("My Note");
      Assert.assertTrue(changes);
    }

    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      cache.removeAllNotes();
      boolean changes = cache.writeNotes("My Note");
      Assert.assertTrue(changes);
    }

    {
      NoteMapCache cache = new NoteMapCache(git, revWalk);
      BranchNotes note1 = cache.getNote(firstCommitId);
      Assert.assertTrue(note1.isEmpty());
      BranchNotes note2 = cache.getNote(secondCommitId);
      Assert.assertTrue(note2.isEmpty());
    }
  }

}
