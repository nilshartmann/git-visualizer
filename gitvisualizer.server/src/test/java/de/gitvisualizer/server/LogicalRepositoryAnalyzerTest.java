package de.gitvisualizer.server;

import de.gitvisualizer.api.LogicalBranchConf;
import de.gitvisualizer.api.RepositoryAnalyzeConf;
import de.gitvisualizer.api.repository.LogicalBranch;
import de.gitvisualizer.api.repository.LogicalRepository;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.junit.RepositoryTestCase;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LogicalRepositoryAnalyzerTest extends RepositoryTestCase {

  private Git git;

  private String firstCommitId;

  private List<String> featureF1FirstChunkCommitIds;

  private List<String> featureF2CommitIds;

  private String featureF1FirstMergeCommit;

  private String featureF2MergeCommit;

  private List<String> featureF1SecondChunkCommitIds;

  private String featureF1SecondMergeCommit;

  private List<String> release1FirstChunkCommitIds;

  private String devFirstMergeCommit;

  private List<String> releaseF3CommitIds;

  private String releaseFirstMergeCommit;

  private String masterFirstMergeCommit;

  private String devSecondMergeCommit;

  @Before
  public void initGit() throws IOException, GitAPIException {
    git = new Git(db);

    firstCommitId = commitFile("common.txt", "common", "master").name();
    createBranch("dev");
    featureF1FirstChunkCommitIds = createBranchWithCommits("f1", "dev", 3);
    tick();
    featureF2CommitIds = createBranchWithCommits("f2", "dev", 3);
    tick();

    featureF1FirstMergeCommit = mergeBranch("f1", "dev");
    tick();
    featureF2MergeCommit = mergeBranch("f2", "dev");
    tick();

    featureF1SecondChunkCommitIds = createCommits("f1", 4);
    tick();
    featureF1SecondMergeCommit = mergeBranch("f1", "dev");
    tick();
    release1FirstChunkCommitIds = createBranchWithCommits("release/rel1", "dev", 3);
    tick();
    devFirstMergeCommit = mergeBranch("release/rel1", "dev");
    tick();
    releaseF3CommitIds = createBranchWithCommits("rel/f3", "release/rel1", 3);
    tick();
    releaseFirstMergeCommit = mergeBranch("rel/f3", "release/rel1");
    tick();

    masterFirstMergeCommit = mergeBranch("release/rel1", "master");
    tick();
    devSecondMergeCommit = mergeBranch("release/rel1", "dev");
    tick();
  }

  private List<String> createBranchWithCommits(String branch, String startBranch, int commits) throws IOException {
    createBranch(branch);
    return createCommits(branch, commits);
  }

  private void createBranch(String branch) throws IOException {
    createBranch(db.resolve(Constants.HEAD), "refs/heads/" + branch);
  }

  private List<String> createCommits(String branch, int commits) {
    return IntStream.range(0, commits).mapToObj(i -> {
      tick();
      return commitFile(branch + ".txt", UUID.randomUUID().toString(), branch).toObjectId().name();
    }).collect(Collectors.toList());

  }

  private String mergeBranch(String branch, String into) throws IOException, GitAPIException {
    checkoutBranch("refs/heads/" + into);
    MergeResult mergeResult = git.merge().setFastForward(MergeCommand.FastForwardMode.NO_FF).include(db.getRef(branch))
                                 .setCommit(false).call();
    return git.commit().setCommitter(committer).setAuthor(author).call().name();
  }

  @Test
  public void testReadBranches() throws IOException, GitAPIException {
    RepositoryAnalyzeConf analyzeConf = new RepositoryAnalyzeConf(
        Arrays.asList(new LogicalBranchConf("master", "master", 10),
                      new LogicalBranchConf("release", "release/.+", 0),
                      new LogicalBranchConf("dev", "dev", 5)),
        true);

    try (GitRepository gitRepository = new GitRepository(git, analyzeConf)) {
      LogicalRepositoryAnalyzer la = new LogicalRepositoryAnalyzer(gitRepository);
      LogicalRepository logicalRepository = la.analyze();
      Assert.assertNotNull(logicalRepository);
      List<LogicalBranch> logicalBranches = logicalRepository.getLogicalBranches();
      Assert.assertEquals(3, logicalBranches.size());
      LogicalBranch masterBranch = logicalBranches.get(0);
      Assert.assertEquals("master", masterBranch.getName());
      LogicalBranch releaseBranch = logicalBranches.get(1);
      Assert.assertEquals("release", releaseBranch.getName());
      LogicalBranch devBranch = logicalBranches.get(2);
      Assert.assertEquals("dev", devBranch.getName());

      Assert.assertEquals(2, masterBranch.getBranchItems().size());
      Assert.assertEquals(4, releaseBranch.getBranchItems().size());
      Assert.assertEquals(5, devBranch.getBranchItems().size());
    }

  }

  /**
   * Origin method does not respect committer and author
   */
  protected RevCommit commitFile(String filename, String contents, String branch) {
    try {
      Git git = new Git(db);
      Repository repo = git.getRepository();
      String originalBranch = repo.getFullBranch();
      boolean empty = repo.resolve(Constants.HEAD) == null;
      if (!empty) {
        if (repo.getRef(branch) == null)
          git.branchCreate().setName(branch).call();
        git.checkout().setName(branch).call();
      }

      writeTrashFile(filename, contents);
      git.add().addFilepattern(filename).call();
      RevCommit commit = git.commit().setAuthor(author).setCommitter(committer)
                            .setMessage(branch + ": " + filename).call();

      if (originalBranch != null)
        git.checkout().setName(originalBranch).call();
      else if (empty)
        git.branchCreate().setName(branch).setStartPoint(commit).call();

      return commit;
    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (GitAPIException e) {
      throw new RuntimeException(e);
    }
  }
}
