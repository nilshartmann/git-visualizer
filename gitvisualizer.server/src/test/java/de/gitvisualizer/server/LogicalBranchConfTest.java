package de.gitvisualizer.server;

import org.junit.Assert;
import org.junit.Test;

import de.gitvisualizer.api.LogicalBranchConf;

public class LogicalBranchConfTest {
  @Test
  public void testInitWithSimpleName() {
    LogicalBranchConf desc = new LogicalBranchConf("master");
    Assert.assertEquals("master", desc.getName());
    Assert.assertEquals("master", desc.getBranchPatterns().get(0));

  }

  @Test
  public void testInitWithStrictFirstParentSimpleName() {
    LogicalBranchConf desc = new LogicalBranchConf("!master");
    Assert.assertEquals("master", desc.getName());
    Assert.assertEquals("master", desc.getBranchPatterns().get(0));

  }

  @Test
  public void testInitWithSimpleNameAndMissingPattern() {
    LogicalBranchConf desc = new LogicalBranchConf("master:");
    Assert.assertEquals("master", desc.getName());
    Assert.assertEquals("master", desc.getBranchPatterns().get(0));

  }

  @Test(expected = IllegalArgumentException.class)
  public void testInitWithTooManyParts() {
    new LogicalBranchConf("master:master/*:dfsd");
  }
}
