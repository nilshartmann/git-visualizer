package de.gitvisualizer.api.repository.items;

public class LogicalTag {
  private final String name;

  public LogicalTag(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

}
