package de.gitvisualizer.api.repository;

public class BranchItemConnection extends RepositoryItem {

  private final String connectionType;

  private final String fromBranchItemId;

  private final String toBranchItemId;

  public BranchItemConnection(String connectionType, String fromBranchItemId, String toBranchItemId) {
    this.connectionType = connectionType;
    this.fromBranchItemId = fromBranchItemId;
    this.toBranchItemId = toBranchItemId;
  }

  public String getConnectionType() {
    return connectionType;
  }

  public String getFromBranchItemId() {
    return fromBranchItemId;
  }

  public String getToBranchItemId() {
    return toBranchItemId;
  }

  public String toShortString() {
    return fromBranchItemId + " -> " + toBranchItemId;
  }

}
