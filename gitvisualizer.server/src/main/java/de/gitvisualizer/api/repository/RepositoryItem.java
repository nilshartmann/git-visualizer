package de.gitvisualizer.api.repository;

/**
 * (Abstract) superclass of all items contained in a repository
 *
 */
public abstract class RepositoryItem {
  private final String type;

  protected RepositoryItem() {
    this.type = getClass().getSimpleName();
  }

  public String getType() {
    return this.type;
  }

}
