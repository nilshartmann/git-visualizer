package de.gitvisualizer.api.repository.items;

import java.util.Collections;
import java.util.List;

import de.gitvisualizer.api.repository.BranchItem;
import de.gitvisualizer.api.repository.CommitDescription;

public class SingleCommit extends BranchItem {
  private final CommitDescription commitDescription;

  private final List<LogicalTag>  logicalTags;

  public SingleCommit(String id, CommitDescription commitDescription, List<LogicalTag> logicalTags) {
    super(id);

    this.commitDescription = commitDescription;
    this.logicalTags = Collections.unmodifiableList(logicalTags);
  }

  public CommitDescription getCommitDescription() {
    return commitDescription;
  }

  public List<LogicalTag> getLogicalTags() {
    return logicalTags;
  }

}
