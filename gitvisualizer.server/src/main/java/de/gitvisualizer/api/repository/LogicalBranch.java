package de.gitvisualizer.api.repository;

import java.util.Collections;
import java.util.List;

public class LogicalBranch extends RepositoryItem {

  private final String           name;

  private final List<BranchItem> branchItems;

  public LogicalBranch(String name, List<BranchItem> branchItems) {
    super();
    this.name = name;
    this.branchItems = Collections.unmodifiableList(branchItems);
  }

  public String getName() {
    return name;
  }

  public List<BranchItem> getBranchItems() {
    return branchItems;
  }

  public String toShortString() {
    StringBuilder b = new StringBuilder();

    for (BranchItem branchItem : branchItems) {
      if (b.length() > 0) {
        b.append("--");
      }
      b.append("(").append(branchItem.toShortString()).append(")");
    }

    b.append(']');

    return name + ": " + b.toString();
  }
}
