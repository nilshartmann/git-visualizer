package de.gitvisualizer.api.repository.items;

import java.util.Collections;
import java.util.List;

import de.gitvisualizer.api.repository.BranchItem;
import de.gitvisualizer.api.repository.CommitDescription;

public class CombinedCommit extends BranchItem {

  private final List<CommitDescription> commitDescriptions;

  public CombinedCommit(String id, List<CommitDescription> commitDescriptions) {
    super(id);
    this.commitDescriptions = Collections.unmodifiableList(commitDescriptions);
  }

  public List<CommitDescription> getCommitDescriptions() {
    return commitDescriptions;
  }

  @Override
  public String toShortString() {

    StringBuilder b = new StringBuilder();
    for (CommitDescription commitDescription : commitDescriptions) {
      if (b.length() > 0) {
        b.append(',');
      }
      b.append(commitDescription.getSha());
    }

    return getType() + ":" + getId() + "{" + b + "}";
  }
}
