package de.gitvisualizer.api.repository.items;

import de.gitvisualizer.api.repository.BranchItem;

public class BranchStart extends BranchItem {

  private final String name;

  public BranchStart(String id, String name) {
    super(id);
    this.name = name;
  }

  public String getName() {
    return name;
  }

}
