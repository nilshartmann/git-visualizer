package de.gitvisualizer.api.repository;

import java.util.Collections;
import java.util.List;

public class CommitDescription {

  private final String       sha;

  private final String       shortMessage;

  private final String       fullMessage;

  private final List<String> tags;

  public CommitDescription(String sha, String shortMessage, String fullMessage, List<String> tags) {
    super();
    this.sha = sha;
    this.shortMessage = shortMessage;
    this.fullMessage = fullMessage;
    this.tags = Collections.unmodifiableList(tags);
  }

  public String getSha() {
    return sha;
  }

  public String getShortMessage() {
    return shortMessage;
  }

  public String getFullMessage() {
    return fullMessage;
  }

  public List<String> getTags() {
    return tags;
  }

}
