package de.gitvisualizer.api.repository;

/**
 * Baseclass for all items that may exist on a LogicalBranch
 * 
 * @author nils
 *
 */
public abstract class BranchItem extends RepositoryItem {

  private final String id;

  protected BranchItem(String id) {
    this.id = id;
    // huestel...

  }

  /** Über alle Items eindeutige Id. */
  public String getId() {
    return this.id;

  }

  public String toShortString() {
    return getType() + ":" + getId();
  }

}
