package de.gitvisualizer.api.repository;

import de.gitvisualizer.server.NoteMapCache;

import java.util.Collections;
import java.util.List;

public class LogicalRepository {

  private final List<LogicalBranch>        logicalBranches;

  private final List<BranchItemConnection> branchItemConnections;

  public LogicalRepository(List<LogicalBranch> logicalBranches, List<BranchItemConnection> branchItemConnections) {
    super();
    this.logicalBranches = Collections.unmodifiableList(logicalBranches);
    this.branchItemConnections = Collections.unmodifiableList(branchItemConnections);
  }

  public List<LogicalBranch> getLogicalBranches() {
    return logicalBranches;
  }

  public List<BranchItemConnection> getBranchItemConnections() {
    return branchItemConnections;
  }

  public String toShortString() {

    StringBuilder builder = new StringBuilder();

    for (LogicalBranch logicalBranch : logicalBranches) {
      builder.append("LogicalBranch: " + logicalBranch.toShortString() + "\n");
    }

    for (BranchItemConnection branchItemConnection : branchItemConnections) {
      builder.append("Connect: " + branchItemConnection.toShortString() + "\n");
    }

    return builder.toString();

  }

}
