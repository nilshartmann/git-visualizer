package de.gitvisualizer.api;

import java.util.ArrayList;
import java.util.List;

public class MergeCommitPattern {
  private String pattern;
  private int    fromGroup;
  private int    toGroup;
  private String defaultFromBranch;
  private String defaultToBranch;

  public MergeCommitPattern() {
  }

  public MergeCommitPattern(String pattern, int fromGroup, int toGroup) {
    this.pattern = pattern;
    this.fromGroup = fromGroup;
    this.toGroup = toGroup;
  }

  public MergeCommitPattern(String pattern, String defaultFromBranch, int toGroup) {
    this.pattern = pattern;
    this.defaultFromBranch = defaultFromBranch;
    this.toGroup = toGroup;
  }

  public MergeCommitPattern(String pattern,int fromGroup, String defaultToBranch) {
    this.pattern = pattern;
    this.defaultToBranch = defaultToBranch;
    this.fromGroup= fromGroup;
  }

  public String getPattern() {
    return pattern;
  }

  public int getFromGroup() {
    return fromGroup;
  }

  public int getToGroup() {
    return toGroup;
  }

  public String getDefaultFromBranch() {
    return defaultFromBranch;
  }

  public String getDefaultToBranch() {
    return defaultToBranch;
  }
}
