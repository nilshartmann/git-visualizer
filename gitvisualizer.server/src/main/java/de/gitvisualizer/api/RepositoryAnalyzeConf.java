package de.gitvisualizer.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RepositoryAnalyzeConf {
  public final static List<MergeCommitPattern> DEFAULT_MERGE_PATTERN;
  static {
    DEFAULT_MERGE_PATTERN=new ArrayList<>();
    DEFAULT_MERGE_PATTERN.add(new MergeCommitPattern("Merge remote-tracking branch 'origin/(\\S+)'",1,1));
    DEFAULT_MERGE_PATTERN.add(new MergeCommitPattern("Merge branch '(\\S+)'",1,"master"));
    DEFAULT_MERGE_PATTERN.add(new MergeCommitPattern("Merge branch '(\\S+)' into (\\S+)",1,2));
  }

  private Boolean                  useDefaultMergeCommitPatterns;
  private List<LogicalBranchConf>  branches;
  private List<MergeCommitPattern> mergeCommitPatterns;

  public RepositoryAnalyzeConf() {
    this.branches = Collections.emptyList();
    this.mergeCommitPatterns = Collections.emptyList();
  }

  public RepositoryAnalyzeConf(List<LogicalBranchConf> branches, List<MergeCommitPattern> mergeCommitPatterns) {
    this.branches = new ArrayList<>(branches);
    this.mergeCommitPatterns = new ArrayList<>(mergeCommitPatterns);
  }

  public RepositoryAnalyzeConf(List<LogicalBranchConf> branches, boolean useDefaultMergeCommitPatterns) {
    this.branches = new ArrayList<>(branches);
    this.useDefaultMergeCommitPatterns = useDefaultMergeCommitPatterns;
  }

  public List<LogicalBranchConf> getBranches() {
    return branches!=null ? branches : Collections.emptyList();
  }

  public List<MergeCommitPattern> getMergeCommitPatterns() {
    return mergeCommitPatterns!=null ? mergeCommitPatterns : Collections.emptyList();
  }

  public List<MergeCommitPattern> getAllMergeCommitPatterns() {
    if(!isUseDefaultMergeCommitPatterns()) {
      return getMergeCommitPatterns();
    }
    return Stream.concat(getMergeCommitPatterns().stream(),DEFAULT_MERGE_PATTERN.stream()).collect(Collectors.toList());
  }

  public boolean isUseDefaultMergeCommitPatterns() {
    return useDefaultMergeCommitPatterns != null && useDefaultMergeCommitPatterns.booleanValue();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    RepositoryAnalyzeConf that = (RepositoryAnalyzeConf) o;

    if (branches != null ? !branches.equals(that.branches) : that.branches != null) return false;
    if (mergeCommitPatterns != null ? !mergeCommitPatterns
        .equals(that.mergeCommitPatterns) : that.mergeCommitPatterns != null) return false;
    if (useDefaultMergeCommitPatterns != null ? !useDefaultMergeCommitPatterns
        .equals(that.useDefaultMergeCommitPatterns) : that.useDefaultMergeCommitPatterns != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = useDefaultMergeCommitPatterns != null ? useDefaultMergeCommitPatterns.hashCode() : 0;
    result = 31 * result + (branches != null ? branches.hashCode() : 0);
    result = 31 * result + (mergeCommitPatterns != null ? mergeCommitPatterns.hashCode() : 0);
    return result;
  }
}
