package de.gitvisualizer.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LogicalBranchConf {
  private String name;

  private List<String> branchPatterns;

  private Integer priority;

  public LogicalBranchConf() {
  }

  public LogicalBranchConf(String name, int priority) {
    this(name,(List<String>)null,priority);
  }

  public LogicalBranchConf(String name, String branchPattern,int priority) {
    this(name,Collections.singletonList(branchPattern),priority);
  }

  public LogicalBranchConf(String name, List<String> branchPatterns,int priority) {
    this.name = name;

    this.branchPatterns =  branchPatterns!=null ? new ArrayList<>(branchPatterns) : null;
    this.priority = priority;
  }

  public LogicalBranchConf(String spec) {
    String[] parts = spec.split(":");
    if (parts.length==0 || parts[0].isEmpty()) {
      throw new IllegalArgumentException("Name missing");
    }
    if (parts.length > 2) {
      throw new IllegalArgumentException("Too many parts");
    }

    parseNameAndStrict(parts[0]);
    if(parts.length>1) {
      this.branchPatterns =  Collections.singletonList(parts[1]);
    }
  }

  private void parseNameAndStrict(String name) {
    if (name.startsWith("!")) {
      this.name = name.substring(1);
    } else {
      this.name = name;
    }
  }


  public String getName() {
    return name;
  }

  public List<String> getBranchPatterns() {
    return branchPatterns!=null && !branchPatterns.isEmpty() ? branchPatterns :  Collections.singletonList(name);
  }

  public Integer getPriority() {
    return priority;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((priority == null) ? 0 : priority.hashCode());
    result = prime * result + ((branchPatterns == null) ? 0 : branchPatterns.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    LogicalBranchConf other = (LogicalBranchConf) obj;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (priority == null) {
      if (other.priority != null)
        return false;
    } else if (!priority.equals(other.priority))
      return false;
    if (branchPatterns == null) {
      if (other.branchPatterns != null)
        return false;
    } else if (!branchPatterns.equals(other.branchPatterns))
      return false;
    return true;
  }
}
