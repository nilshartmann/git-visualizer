package de.gitvisualizer.api;

public class NewRepository {
  private String                url;

  private RepositoryAnalyzeConf analyzeConf;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public RepositoryAnalyzeConf getAnalyzeConf() {
    return analyzeConf;
  }

  public void setAnalyzeConf(RepositoryAnalyzeConf analyzeConf) {
    this.analyzeConf = analyzeConf;
  }
}
