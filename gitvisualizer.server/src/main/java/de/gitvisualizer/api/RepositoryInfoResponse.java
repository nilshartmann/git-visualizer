package de.gitvisualizer.api;

public class RepositoryInfoResponse {
  private String                name;

  private String                url;

  private RepositoryAnalyzeConf analyzeConf;

  public RepositoryInfoResponse() {
  }

  public RepositoryInfoResponse(String name, String url, RepositoryAnalyzeConf analyzeConf) {
    this.name = name;
    this.url = url;
    this.analyzeConf = analyzeConf;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public RepositoryAnalyzeConf getAnalyzeConf() {
    return analyzeConf;
  }

  public void setAnalyzeConf(RepositoryAnalyzeConf analyzeConf) {
    this.analyzeConf = analyzeConf;
  }

}
