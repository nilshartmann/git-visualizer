package de.gitvisualizer.server;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Constants;

import de.gitvisualizer.api.RepositoryAnalyzeConf;

public class GitRepository implements Closeable{
  private Git                   git;

  private RepositoryAnalyzeConf analyzeConf;

  public GitRepository(Git git, RepositoryAnalyzeConf analyzeConf) {
    this.git = git;
    this.analyzeConf = analyzeConf;

  }

  public Git getGit() {
    return git;
  }

  public String getName() {
    return git.getRepository().getDirectory().getName();
  }

  public File getDir() {
    return git.getRepository().getDirectory();
  }

  public String getUrl() {
    return git.getRepository().getConfig().getString("remote", Constants.DEFAULT_REMOTE_NAME, "url");
  }

  public RepositoryAnalyzeConf getAnalyzeConf() {
    return analyzeConf;
  }

  @Override
  public void close() {
    git.close();
  }
}
