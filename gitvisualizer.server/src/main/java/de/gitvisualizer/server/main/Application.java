package de.gitvisualizer.server.main;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.gitvisualizer.server.RepositoryAnalyzeConfHandler;
import de.gitvisualizer.server.RepositoryService;
import de.gitvisualizer.server.controller.APIController;

@ComponentScan(basePackageClasses = { APIController.class })
@EnableAutoConfiguration
@Configuration
public class Application {

  @Value("${reposLocation}")
  private File reposLocation;

  @Autowired
  private ObjectMapper objectMapper;

  public static File getProjectRootDir() {
    URL url = Application.class.getProtectionDomain().getCodeSource().getLocation();

    File binDir;
    try {
      binDir = new File(url.toURI());
    } catch (URISyntaxException e) {
      throw new IllegalStateException(e);
    }
    return binDir.getParentFile();
  }

  public RepositoryAnalyzeConfHandler repositoryAnalyzeConfHandler() {
    return new RepositoryAnalyzeConfHandler(objectMapper);
  }

  @Bean
  public RepositoryService repositoryService() {
    return new RepositoryService(reposLocation, repositoryAnalyzeConfHandler());
  }

  @Configuration
  public static class StaticResourceConfiguration extends WebMvcConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(StaticResourceConfiguration.class);
    public static final String GITVISUALIZER_CLIENT = "gitvisualizer.client";

    public static boolean isDevelopRun() throws IOException {
      return isRunFromRootDir() != null || isRunFromServerDir() != null;
    }

    private static File isRunFromRootDir() throws IOException {
      File client = Paths.get(GITVISUALIZER_CLIENT).toFile().getCanonicalFile();
      if (client.exists() && client.isDirectory()) {
        return client;
      }
      return null;
    }

    private static File isRunFromServerDir() throws IOException {
      File client = Paths.get("../" + GITVISUALIZER_CLIENT).toFile().getCanonicalFile();
      if (client.exists() && client.isDirectory()) {
        return client;
      }
      return null;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
      try {
        File client = isRunFromRootDir();
        if (client == null) {
          client = isRunFromServerDir();
        }
        if (client != null) {
          String url = String.format("file:%s/", client.getAbsoluteFile().toString());
          LOGGER.info("Add static file mapper: " + url);
          registry.addResourceHandler("/**").addResourceLocations(url);
        }
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  public static void main(String[] args) throws IOException {
    if (StaticResourceConfiguration.isDevelopRun()) {
      SpringApplication.run(new Object[] { Application.class, StaticResourceConfiguration.class }, args);
    } else {
      SpringApplication.run(Application.class, args);
    }

  }
}
