package de.gitvisualizer.server;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.notes.Note;
import org.eclipse.jgit.notes.NoteMap;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;

import java.io.IOException;
import java.util.*;

public class NoteMapCache {
  private static final String NOTES_REF = Constants.R_NOTES_COMMITS;

  private final Git git;
  private final Map<RevCommit, BranchNotes> commitToNoteMap = new HashMap<>();
  private RevWalk  revWalk;
  private NoteMap  noteMap;
  private ObjectId notesCommitId;

  private boolean removedNote = false;

  public NoteMapCache(Git git, RevWalk revWalk) {
    this.git = git;
    this.revWalk = revWalk;
    initNoteMap();
  }

  public void removeAllNotes() {
    commitToNoteMap.clear();

    List<Note> notesToRemove = new ArrayList<>();
    for (Note note : noteMap) {
      notesToRemove.add(note);
    }

    removedNote = removedNote || !notesToRemove.isEmpty();

    for (Note note : notesToRemove) {
      try {
        noteMap.remove(note);
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  public Collection<RevCommit> getCommitsWithNotes() {
    Map<String, RevCommit> id2CommitMap = new HashMap<>();
    commitToNoteMap.keySet().forEach(revCommit -> id2CommitMap.put(revCommit.name(), revCommit));

    for (Note note : noteMap) {
      if (!id2CommitMap.containsKey(note.name())) {
        try {
          RevCommit revCommit = revWalk.parseCommit(note);
          id2CommitMap.put(revCommit.name(), revCommit);
        } catch (IOException e) {
          throw new IllegalStateException(e);
        }
      }
    }

    return id2CommitMap.values();
  }

  public BranchNotes getNote(RevCommit commit) {
    BranchNotes note = commitToNoteMap.get(commit);
    if (note == null) {
      try {
        note = new BranchNotes();
        Note rawNote = noteMap.getNote(commit.getId());
        if (rawNote != null) {
          ObjectId dataId = rawNote.getData();
          ObjectLoader objectLoader = revWalk.getObjectReader().open(dataId);
          try (ObjectStream objectStream = objectLoader.openStream()) {
            note.readFromRawData(objectStream);
          }
        }
        commitToNoteMap.put(commit, note);
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    }
    return note;
  }

  public boolean writeNotes(String message) {
    boolean changed = removedNote;
    this.removedNote = false;
    ObjectInserter inserter = git.getRepository().newObjectInserter();
    try {
      for (RevCommit commit : commitToNoteMap.keySet()) {
        BranchNotes note = getNote(commit);
        if (!note.isChanged()) {
          continue;
        }
        if (note.isEmpty()) {
          noteMap.remove(commit);
        } else {
          noteMap.set(commit, note.toRawData(), inserter);
        }
        note.markAsUnchanged();
        changed = true;
      }

      if (changed) {
        CommitBuilder builder = new CommitBuilder();
        builder.setTreeId(noteMap.writeTree(inserter));
        builder.setAuthor(new PersonIdent(git.getRepository()));
        builder.setCommitter(builder.getAuthor());
        builder.setMessage(message);
        if (notesCommitId != null)
          builder.setParentIds(notesCommitId);
        ObjectId newNotesCommitId = inserter.insert(builder);
        inserter.flush();
        RefUpdate refUpdate = git.getRepository().updateRef(NOTES_REF);
        if (notesCommitId != null)
          refUpdate.setExpectedOldObjectId(notesCommitId);
        else
          refUpdate.setExpectedOldObjectId(ObjectId.zeroId());
        refUpdate.setNewObjectId(newNotesCommitId);
        refUpdate.update();
        this.notesCommitId = newNotesCommitId;
      }
    } catch (IOException e) {
      throw new IllegalStateException(e);
    } finally {
      inserter.release();
    }

    return true;
  }

  private void initNoteMap() {
    try {
      Ref ref = git.getRepository().getRef(NOTES_REF);
      if (ref != null) {
        RevCommit notesCommit = revWalk.parseCommit(ref.getObjectId());
        this.notesCommitId = notesCommit.getId();
        this.noteMap = NoteMap.read(revWalk.getObjectReader(), notesCommit);
      } else {
        this.noteMap = NoteMap.newEmptyMap();
      }
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

}
