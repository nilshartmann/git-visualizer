package de.gitvisualizer.server;


import com.fasterxml.jackson.databind.ObjectMapper;
import de.gitvisualizer.api.RepositoryAnalyzeConf;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class RepositoryAnalyzeConfHandler {
  private static final String ANALYZE_CONF_FILE = "analyze.conf.json";
  private final ObjectMapper objectMapper;

  public RepositoryAnalyzeConfHandler(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public RepositoryAnalyzeConf readFromRepo(File dir) {
    File anaylzeConfFile = new File(dir, ANALYZE_CONF_FILE);
    if (anaylzeConfFile.exists()) {
      try (Reader reader = new FileReader(anaylzeConfFile)) {
        return objectMapper.reader(RepositoryAnalyzeConf.class).readValue(reader);
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }

    }
    return null;
  }

  public void writeToRepo(File dir, RepositoryAnalyzeConf analyzeConf) {
    File analyzeConfFile = new File(dir, ANALYZE_CONF_FILE);
    try {
      objectMapper.writerWithType(RepositoryAnalyzeConf.class).writeValue(analyzeConfFile, analyzeConf);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }
}
