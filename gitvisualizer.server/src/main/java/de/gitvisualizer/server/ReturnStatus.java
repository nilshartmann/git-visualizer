package de.gitvisualizer.server;

public class ReturnStatus<S extends Enum<?>, T> {
  final private S status;

  final private T result;

  public ReturnStatus(S status) {
    this(status, null);
  }

  public ReturnStatus(S status, T result) {
    this.status = status;
    this.result = result;
  }

  public S getStatus() {
    return status;
  }

  public T getResult() {
    return result;
  }

}
