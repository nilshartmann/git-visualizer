package de.gitvisualizer.server;

import org.eclipse.jgit.revwalk.RevCommit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class BranchNotes {
  private static final String REAL_BRANCH_PREFIX = "real#";

  private static final String PROP_BRANCH                 = "BRANCH";
  private static final String PROP_MERGE_DESTINATION_INFO = "MERGE-DESTINATION-INFO";
  private static final String PROP_MERGE_SOURCE           = "MERGE-SOURCE";
  private static final String PROP_CHILD_TEMPLATE         = "CHILD-%d";

  private static final String PROP_START_INFO   = "START-INFO";
  private static final String PROP_START_SOURCE = "START-SOURCE";

  private static final Set<String> NO_SIMPLE_COMMIT_PROPS = new HashSet<>(
      Arrays.asList(PROP_MERGE_DESTINATION_INFO, PROP_MERGE_SOURCE, PROP_START_SOURCE));

  private Map<String, String> notes = new HashMap<>();

  private boolean changed = false;

  public BranchNotes() {
  }

  public void readFromRawData(InputStream inputStream) {
    reset();
    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
    String line;
    try {
      while ((line = reader.readLine()) != null) {
        int splitPoint = line.indexOf('=');
        if (splitPoint < 0) {
          throw new IllegalArgumentException("Invalid format of line: " + line);
        }

        String key = line.substring(0, splitPoint);
        String value = line.substring(splitPoint + 1);
        notes.put(key, value);
      }
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  private void reset() {
    notes.clear();
    changed = false;
  }

  public boolean isChanged() {
    return changed;
  }

  public String getBranchname() {
    return getProperty(PROP_BRANCH);
  }

  public String getLogicalBranchname() {
    String branchname = getProperty(PROP_BRANCH);
    if(branchname==null || branchname.startsWith(REAL_BRANCH_PREFIX)) {
      return null;
    }
    return branchname;
  }

  public void setLogicalBranchname(String branch) {
    setProperty(PROP_BRANCH, branch);
  }

  public String getRealBranchname() {
    String branchname = getProperty(PROP_BRANCH);
    if(branchname==null || !branchname.startsWith(REAL_BRANCH_PREFIX)) {
      return null;
    }
    return branchname;
  }

  public void setRealBranchname(String branch) {
    if(branch!=null) {
      branch=REAL_BRANCH_PREFIX+branch;
    }
    setProperty(PROP_BRANCH, branch);
  }

  public void setProperty(String key, String value) {
    if (value == null) {
      String oldValue = notes.remove(key);
      changed = changed && oldValue != null;
    } else {
      String oldValue = notes.put(key, value);
      changed = changed || !Objects.equals(value, oldValue);
    }
  }

  public String getProperty(String key) {
    return notes.get(key);
  }

  public int size() {
    return notes.size();
  }

  public String toRawData() {
    StringBuilder builder = new StringBuilder();
    notes.entrySet().stream()
         .forEach(entry -> builder.append(String.format("%s=%s%n", entry.getKey(), entry.getValue())));

    return builder.toString();
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public void markAsUnchanged() {
    changed = false;
  }

  public void clear() {
    if (notes.isEmpty()) {
      return;
    }

    notes.clear();
    changed = true;

  }

  public boolean isSimpleCommit() {
    for (String prop : NO_SIMPLE_COMMIT_PROPS) {
      if(notes.containsKey(prop)) {
        return false;
      }
    }

    return true;
  }

  public void setMergeDestinationInfo(String fromBranch, String sha) {
    setProperty(PROP_MERGE_DESTINATION_INFO, fromBranch + ":" + sha);
  }

  public String[] getMergeDestinationInfo() {
    String property = getProperty(PROP_MERGE_DESTINATION_INFO);
    if (property != null) {
      return property.split(":");
    }
    return null;
  }

  public void markAsMergeSource() {
    setProperty(PROP_MERGE_SOURCE, Boolean.TRUE.toString());
  }

  public boolean isMergeSource() {
    String property = getProperty(PROP_MERGE_SOURCE);
    return property != null && Boolean.parseBoolean(property);
  }

  public void setStartInfo(String fromBranch, String sha) {
    setProperty(PROP_START_INFO, fromBranch + ":" + sha);
  }

  public String[] getStartInfo() {
    String property = getProperty(PROP_START_INFO);
    if (property != null) {
      return property.split(":");
    }
    return null;
  }

  public void markAsStartSource() {
    setProperty(PROP_START_SOURCE, Boolean.TRUE.toString());
  }

  public boolean isStartSource() {
    String property = getProperty(PROP_START_SOURCE);
    return property != null && Boolean.parseBoolean(property);
  }

  public void addChild(int parentNumberOfChild, RevCommit childCommit) {
    String childProp=String.format(PROP_CHILD_TEMPLATE,parentNumberOfChild);
    String childValue = getProperty(childProp);
    if(childValue==null) {
      childValue=childCommit.name();
    } else {
      childValue=childValue+","+childCommit.name();
    }
    setProperty(childProp,childValue);
  }

}
