package de.gitvisualizer.server;

import org.eclipse.jgit.errors.StopWalkException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.RevFilter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class FirstParentRevFilter extends RevFilter {

  private Set<RevCommit> firstParents;

  @Override
  public boolean include(RevWalk walker, RevCommit cmit) throws StopWalkException,
      IOException {
    if (firstParents == null) {
      firstParents = new HashSet<>();
      if (cmit.getParentCount() > 0) {
        firstParents.add(cmit.getParent(0));
      }
      return true;
    }

    if (firstParents.contains(cmit)) {
      if (cmit.getParentCount() > 0) {
        firstParents.add(cmit.getParent(0));
      }
      return true;
    }
    return false;
  }

  @SuppressWarnings("CloneDoesntCallSuperClone")
  @Override
  public RevFilter clone() {
    FirstParentRevFilter newClone = new FirstParentRevFilter();
    if (firstParents == null) {
      return newClone;
    }
    newClone.firstParents = new HashSet<>(firstParents);
    return newClone;
  }

}
