package de.gitvisualizer.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.jgit.lib.Ref;

import de.gitvisualizer.api.LogicalBranchConf;

public class LogicalBranchDesc implements Comparable<LogicalBranchDesc> {
  private LogicalBranchConf logicalBranchConf;

  private List<Pattern> branchPatterns;

  private List<Ref> realBranches = new ArrayList<Ref>();

  public LogicalBranchDesc(String name, String pattern, int priority) {
    this(new LogicalBranchConf(name, pattern, priority));
  }

  public LogicalBranchDesc(LogicalBranchConf logicalBranchConf) {
    this.logicalBranchConf = logicalBranchConf;

    branchPatterns = logicalBranchConf.getBranchPatterns().stream().map(rawPattern -> Pattern.compile(rawPattern))
                                      .collect(
                                          Collectors.toList());

  }

  public String getName() {
    return logicalBranchConf.getName();
  }

  public List<Pattern> getBranchPatterns() {
    return branchPatterns;
  }

  public boolean matches(String branch) {
    return branchPatterns.stream().filter(pattern -> pattern.matcher(Branches.shortName(branch)).matches()).findFirst().isPresent();
  }

  public List<Ref> getRealBranches() {
    return realBranches;
  }

  public void extractRealBranches(Map<String, Ref> branchesMap) {
    Iterator<Entry<String, Ref>> iterator = branchesMap.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<String, Ref> entry = iterator.next();
      if (matches(entry.getKey())) {
        realBranches.add(entry.getValue());
        iterator.remove();
      }
    }
  }

  public int getOrder() {
    return logicalBranchConf.getPriority() != null ? logicalBranchConf.getPriority() : 0;
  }

  @Override
  public int compareTo(LogicalBranchDesc o) {
    return o.getOrder() - getOrder();
  }
}
