package de.gitvisualizer.server;

import org.eclipse.jgit.lib.Constants;

public class Branches {

  static public String shortName(final String orgName) {
    String name = orgName;
    if (name.startsWith(Constants.R_REMOTES)) {
      name = name.substring(Constants.R_REMOTES.length());
      int nextSlash = name.indexOf('/');
      if (nextSlash < 0) {
        throw new IllegalStateException("Strange ref name. Missing remote name. " + orgName);
      }

      name = name.substring(nextSlash + 1);
      return name;
    }
    if (name.startsWith(Constants.R_HEADS)) {
      name = name.substring(Constants.R_HEADS.length());
      return name;
    }

    return name;
  }

}
