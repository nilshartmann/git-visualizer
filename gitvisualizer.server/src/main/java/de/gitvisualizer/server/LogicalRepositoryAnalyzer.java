package de.gitvisualizer.server;

import de.gitvisualizer.api.MergeCommitPattern;
import de.gitvisualizer.api.repository.*;
import de.gitvisualizer.api.repository.items.BranchStart;
import de.gitvisualizer.api.repository.items.CombinedCommit;
import de.gitvisualizer.api.repository.items.SingleCommit;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class LogicalRepositoryAnalyzer implements Closeable {


  private final Git           git;
  private final GitRepository gitRepository;
  private final RevWalk       revWalk;
  private final Map<String, Optional<LogicalBranchDesc>> realToLogicalBranchMap       = new HashMap<>();
  private final Map<String, List<RevCommit>>             logicalBranchStartCommitsMap = new HashMap<>();

  public LogicalRepositoryAnalyzer(GitRepository gitRepository) {
    this.gitRepository = gitRepository;
    this.git = this.gitRepository.getGit();
    this.revWalk = new RevWalk(git.getRepository());
  }

  public LogicalRepository analyze() {
    try {
      List<LogicalBranchDesc> branchDescs = gitRepository.getAnalyzeConf().getBranches().stream()
                                                         .map(LogicalBranchDesc::new)
                                                         .collect(Collectors.toList());
      SortedSet<LogicalBranchDesc> sortedBranchDesc = new TreeSet<>(branchDescs);

      List<Ref> localBranches = git.branchList().call();
      List<Ref> remoteBranches = git.branchList().setListMode(ListMode.REMOTE).call();
      Map<String, Ref> branchMap = combineAndRemoveDuplicateBranches(remoteBranches, localBranches);
      sortedBranchDesc.stream().forEach(branch -> branch.extractRealBranches(branchMap));

      NoteMapCache noteMapCache = new NoteMapCache(git, revWalk);
      markCommits(noteMapCache, sortedBranchDesc);

      List<BranchItemConnection> connections = new ArrayList<>();

      List<LogicalBranch> branches = branchDescs.stream()
                                                .map(bd -> createLogicalBranch(bd, connections, noteMapCache))
                                                .collect(Collectors.toList());
      return new LogicalRepository(branches, connections);

    } catch (GitAPIException e) {
      throw new IllegalStateException(e);
    }
  }

  private void markChildren(NoteMapCache noteMapCache, SortedSet<LogicalBranchDesc> sortedBranchDesc) {
    RevWalk walkAll = createRevWalkForAllCommits();

    try {
      StreamSupport.stream(walkAll.spliterator(), false).forEach(revCommit -> {
                                                                   for (int p = 0; p < revCommit
                                                                       .getParentCount(); p++) {
                                                                     RevCommit parent = revCommit.getParent(p);
                                                                     BranchNotes notes = noteMapCache.getNote(parent);
                                                                     notes.addChild(p, revCommit);
                                                                   }
                                                                 }
      );
    } finally {
      walkAll.dispose();
    }
  }

  private LogicalBranch createLogicalBranch(LogicalBranchDesc bd, Collection<BranchItemConnection> connections, NoteMapCache noteMapCache) {
    List<BranchItem> branchItems = new ArrayList<>();

    List<RevCommit> commitsFromBranch = noteMapCache.getCommitsWithNotes().stream().filter(revCommit -> {
      String branchname = noteMapCache.getNote(revCommit).getLogicalBranchname();
      return branchname != null && branchname.equals(bd.getName());
    }).collect(Collectors.toList());

    commitsFromBranch.sort((l, r) -> l.getCommitTime() - r.getCommitTime());

    List<RevCommit> combineCommits = new ArrayList<>();
    for (RevCommit revCommit : commitsFromBranch) {
      BranchNotes notes = noteMapCache.getNote(revCommit);
      if (notes.getStartInfo() != null) {
        if (!combineCommits.isEmpty()) {
          branchItems.add(createCommitItem(combineCommits));
          combineCommits.clear();
        }

        BranchStart branchStart = new BranchStart("start-" + revCommit.name(), notes.getLogicalBranchname());
        branchItems.add(branchStart);
        BranchItemConnection connection = new BranchItemConnection(
            "start from " + notes.getStartInfo()[0] + " to " + notes.getLogicalBranchname(), notes.getStartInfo()[1],
            branchStart.getId());
        connections.add(connection);
      }

      if (notes.isSimpleCommit()) {
        combineCommits.add(revCommit);
        continue;
      }

      if (!combineCommits.isEmpty()) {
        branchItems.add(createCommitItem(combineCommits));
        combineCommits.clear();
      }

      branchItems.add(createCommitItem(Collections.singletonList(revCommit)));

      if (notes.getMergeDestinationInfo() != null) {
        BranchItemConnection connection = new BranchItemConnection(
            "merge from " + notes.getMergeDestinationInfo()[0] + " to " + notes.getLogicalBranchname(),
            notes.getMergeDestinationInfo()[1],
            revCommit.name());
        connections.add(connection);
      }
    }
    if (!combineCommits.isEmpty()) {
      branchItems.add(createCommitItem(combineCommits));
    }

    return new LogicalBranch(bd.getName(), branchItems);
  }

  private BranchItem createCommitItem(List<RevCommit> combineCommits) {
    if (combineCommits.size() == 1) {
      RevCommit revCommit = combineCommits.get(0);
      return new SingleCommit(revCommit.name(),
                              new CommitDescription(revCommit.getId().name(),
                                                    revCommit.getShortMessage(),
                                                    revCommit.getFullMessage(),
                                                    Collections.emptyList()),
                              Collections.emptyList());
    }
    List<CommitDescription> commitDescriptions = combineCommits.stream().map(
        go -> new CommitDescription(go.getId().name(), go.getShortMessage(),
                                    go.getFullMessage(), Collections.emptyList())).collect(Collectors.toList());

    return new CombinedCommit(combineCommits.get(0).name(), commitDescriptions);
  }

  private Map<String, Ref> combineAndRemoveDuplicateBranches(List<Ref> remoteBranches, List<Ref> localBranches) {
    Map<String, Ref> result = new HashMap<>();
    for (Ref ref : localBranches) {
      result.put(Branches.shortName(ref.getName()), ref);
    }
    for (Ref ref : remoteBranches) {
      result.put(Branches.shortName(ref.getName()), ref);
    }

    return result;
  }

  private void markCommits(NoteMapCache noteMapCache, SortedSet<LogicalBranchDesc> branchDescs) {
    noteMapCache.removeAllNotes(); //start with a fresh run
    markChildren(noteMapCache, branchDescs);
    markMergeCommitPatterns(branchDescs, noteMapCache);
    markFirstParents(branchDescs, noteMapCache);
    markMergesBetweenLogicalBranches(noteMapCache);

    noteMapCache.writeNotes("mark commits");
  }

  private void markMergeCommitPatterns(SortedSet<LogicalBranchDesc> branchDescs, NoteMapCache noteMapCache) {
    List<MergeCommitPattern> mergeCommitPatterns = gitRepository.getAnalyzeConf().getAllMergeCommitPatterns();

    List<Pattern> compiledPatterns = mergeCommitPatterns.stream().map(p -> Pattern.compile(p.getPattern()))
                                                        .collect(Collectors.toList());

    RevWalk walkAll = createRevWalkForAllCommits();

    try {
      StreamSupport.stream(walkAll.spliterator(), false).filter(revCommit -> revCommit.getParentCount() > 1)
                   .filter(revCommit -> noteMapCache.getNote(revCommit).getBranchname() == null)
                   .forEach(revCommit -> {
                              for (int i = 0; i < compiledPatterns.size(); i++) {
                                Matcher matcher = compiledPatterns.get(i).matcher(revCommit.getShortMessage());
                                if (matcher.matches()) {
                                  MergeCommitPattern mergeCommitPattern = mergeCommitPatterns.get(i);
                                  markMergePatternCommits(mergeCommitPattern, matcher, revCommit, branchDescs,
                                                          noteMapCache);
                                  break;
                                }
                              }
                            }
                   );
    } finally {
      walkAll.dispose();
    }

  }

  private RevWalk createRevWalkForAllCommits() {
    RevWalk walkAll = new RevWalk(git.getRepository());
    git.getRepository().getAllRefs().values().stream().forEach(
        ref -> {
          try {
            walkAll.markStart(walkAll.parseCommit(ref.getObjectId()));
          } catch (IOException e) {
            throw new IllegalStateException(e);
          }
        }
    );
    return walkAll;
  }

  private void markMergePatternCommits(MergeCommitPattern mergeCommitPattern, Matcher matcher, RevCommit revCommit, SortedSet<LogicalBranchDesc> branchDescs, NoteMapCache noteMapCache) {
    String toBranch = mergeCommitPattern.getDefaultToBranch();
    int toGroup = mergeCommitPattern.getToGroup();
    if (toGroup > 0) {
      toBranch = matcher.group(toGroup);
    }


    if(toBranch!=null) {
      Optional<LogicalBranchDesc> logicalBanch = getLogicalBranch(branchDescs, toBranch);
      if (logicalBanch.isPresent()) {
        noteMapCache.getNote(revCommit).setLogicalBranchname(logicalBanch.get().getName());
        List<RevCommit> revCommitList = logicalBranchStartCommitsMap.get(logicalBanch.get().getName());
        if (revCommitList == null) {
          revCommitList = new ArrayList<>();
          logicalBranchStartCommitsMap.put(logicalBanch.get().getName(), revCommitList);
        }
        revCommitList.add(revCommit);
      } else {
        noteMapCache.getNote(revCommit).setRealBranchname(Branches.shortName(toBranch));
      }
    }

    String fromBranch = mergeCommitPattern.getDefaultFromBranch();
    int fromGroup = mergeCommitPattern.getFromGroup();
    if (fromGroup > 0) {
      fromBranch = matcher.group(fromGroup);
    }
    if(fromBranch!=null) {
      RevCommit fromParent = revCommit.getParent(1);
      BranchNotes parentNotes = noteMapCache.getNote(fromParent);
      if (parentNotes.getBranchname() == null) {
        Optional<LogicalBranchDesc> logicalBanch = getLogicalBranch(branchDescs, fromBranch);
        if (logicalBanch.isPresent()) {
          parentNotes.setLogicalBranchname(logicalBanch.get().getName());
          List<RevCommit> revCommitList = logicalBranchStartCommitsMap.get(logicalBanch.get().getName());
          if (revCommitList == null) {
            revCommitList = new ArrayList<>();
            logicalBranchStartCommitsMap.put(logicalBanch.get().getName(), revCommitList);
          }
          revCommitList.add(revCommit);
        } else {
          parentNotes.setRealBranchname(Branches.shortName(fromBranch));
        }
      }
    }
  }

  private Optional<LogicalBranchDesc> getLogicalBranch(SortedSet<LogicalBranchDesc> branchDescs, String realBranch) {
    Optional<LogicalBranchDesc> logicalBranchDesc = realToLogicalBranchMap.get(realBranch);
    if (logicalBranchDesc == null) {
      logicalBranchDesc = branchDescs.stream().filter(
          bd -> bd.matches(realBranch)).findFirst();
      realToLogicalBranchMap.put(realBranch, logicalBranchDesc);
    }

    return logicalBranchDesc;
  }

  private void markMergesBetweenLogicalBranches(NoteMapCache noteMapCache) {
    for (RevCommit revCommit : noteMapCache.getCommitsWithNotes()) {
      BranchNotes notes = noteMapCache.getNote(revCommit);
      String currentBranchname = notes.getLogicalBranchname();
      if (currentBranchname == null) {
        continue;
      }

      //Analyze for First Commit
      boolean firstCommit = true;
      for (int i = 0; i < revCommit.getParentCount(); i++) {
        RevCommit parent = revCommit.getParent(i);
        String parentBranchname = noteMapCache.getNote(parent).getLogicalBranchname();
        if (parentBranchname != null && parentBranchname.equals(currentBranchname)) {
          firstCommit = false;
        }
      }

      if (firstCommit && revCommit.getParentCount() > 0) {
        RevCommit parent = null;
        String parentBranchname = null;

        for (int i = 0; i < revCommit.getParentCount(); i++) {
          parent = revCommit.getParent(i);
          parentBranchname = noteMapCache.getNote(parent).getLogicalBranchname();
          if (parentBranchname != null) {
            break;
          }
        }
        if (parentBranchname != null) {
          notes.setStartInfo(parentBranchname, parent.name());
          noteMapCache.getNote(parent).markAsStartSource();
          continue; //Ignore this commit even if it is a merge
        }
      }


      //Anaylze for Merge
      for (int i = 0; i < revCommit.getParentCount(); i++) {
        RevCommit parent = revCommit.getParent(i);
        String parentBranchname = noteMapCache.getNote(parent).getLogicalBranchname();
        if (parentBranchname != null && !parentBranchname.equals(currentBranchname)) {
          notes.setMergeDestinationInfo(parentBranchname, parent.name());
          noteMapCache.getNote(parent).markAsMergeSource();
          break;
        }
      }
    }
  }

  private void markFirstParents(SortedSet<LogicalBranchDesc> branchDescs, NoteMapCache noteMapCache) {
    for (LogicalBranchDesc branch : branchDescs) {
      for (Ref realBranch : branch.getRealBranches()) {
        markFirstParents(realBranch.getObjectId(), branch, noteMapCache);
      }

      List<RevCommit> revCommits = logicalBranchStartCommitsMap.get(branch.getName());
      if (revCommits != null) {
        revCommits.stream().forEach(revCommit -> markFirstParents(revCommit, branch, noteMapCache));
      }
    }
  }

  private void markFirstParents(ObjectId go, LogicalBranchDesc branch, NoteMapCache noteMapCache) {
    try {
      RevCommit revCommit;
      while (go != null && (revCommit = revWalk.parseCommit(go)) != null) {
        BranchNotes notes = noteMapCache.getNote(revCommit);
        String branchname = notes.getBranchname();
        if (branchname != null && !branchname.equals(branch.getName())) {
          break;
        }
        notes.setLogicalBranchname(branch.getName());
        go = revCommit.getParentCount() == 0 ? null : revCommit.getParent(0);
      }
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }

  }

  @Override
  public void close() throws IOException {
    revWalk.dispose();
  }
}
