package de.gitvisualizer.server;

import de.gitvisualizer.api.NewRepository;
import de.gitvisualizer.api.RepositoryAnalyzeConf;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.util.FileUtils;

import javax.annotation.PreDestroy;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RepositoryService implements Closeable {
  private final File                         reposLocation;
  private final RepositoryAnalyzeConfHandler repositoryAnalyzeConfHandler;
  final private Map<String, GitRepository> repositories = new ConcurrentHashMap<>();
  final private Lock                       writeMapLock = new ReentrantLock();
  final private Log                        log          = LogFactory.getLog(getClass());

  public RepositoryService(File reposLocation, RepositoryAnalyzeConfHandler repositoryAnalyzeConfHandler) {

    this.reposLocation = reposLocation;
    this.repositoryAnalyzeConfHandler = repositoryAnalyzeConfHandler;

    if (!reposLocation.exists()) {
      boolean success = reposLocation.mkdirs();
      if (!success) {
        throw new IllegalStateException("Could not create repo dir: " + reposLocation);
      }
    }

    File[] repoDirs = reposLocation
        .listFiles(pathname -> pathname.isDirectory() && new File(pathname, Constants.CONFIG).exists());

    for (File file : repoDirs) {
      Git git;
      try {
        git = Git.open(file);
        RepositoryAnalyzeConf analyzeConf = repositoryAnalyzeConfHandler.readFromRepo(file);
        if (analyzeConf == null) {
          log.warn("remove dir because the analyze conf is missing: " + file);
          deleteDir(file);
          continue;
        }
        GitRepository repository = new GitRepository(git, analyzeConf);
        repositories.put(repository.getName(), repository);
      } catch (IOException e) {
        deleteDir(file);
      }
    }
  }

  private void deleteDir(File file) {
    try {
      FileUtils.delete(file, FileUtils.RECURSIVE);
    } catch (IOException exc) {
      log.error("Could not delete directory: " + file);
      throw new IllegalStateException(exc);
    }
  }

  public Collection<GitRepository> listAll() {
    return repositories.values();
  }

  public ReturnStatus<AddStatus, GitRepository> addRepo(String name, NewRepository repo) {
    name = name.toLowerCase();
    if (repo.getAnalyzeConf() == null) {
      repo.setAnalyzeConf(new RepositoryAnalyzeConf());
    }
    writeMapLock.lock();
    try {
      GitRepository gitRepository = repositories.get(name);
      if (gitRepository != null) {
        if (gitRepository.getUrl().equals(repo.getUrl())
            && gitRepository.getAnalyzeConf().equals(repo.getAnalyzeConf())) {
          return new ReturnStatus<>(AddStatus.ADDED, gitRepository);
        }

        return new ReturnStatus<>(AddStatus.ALREADY_EXISTS);
      }

      File repoDir = new File(reposLocation, name);
      if (repoDir.exists()) {
        return new ReturnStatus<>(AddStatus.NAME_NOT_ALLOWED);
      }

      Git git = cloneBareRepo(repoDir, repo.getUrl());
      if (git == null) {
        return new ReturnStatus<>(AddStatus.COULD_NOT_CLONE);
      }

      repositoryAnalyzeConfHandler.writeToRepo(repoDir, repo.getAnalyzeConf());
      gitRepository = new GitRepository(git, repo.getAnalyzeConf());

      repositories.put(gitRepository.getName(), gitRepository);
      return new ReturnStatus<>(AddStatus.ADDED, gitRepository);
    } finally {
      writeMapLock.unlock();
    }
  }

  private Git cloneBareRepo(File repoDir, String url) {
    try {
      return Git.cloneRepository().setBare(true).setURI(url).setDirectory(repoDir).call();
    } catch (InvalidRemoteException e) {
      return null;
    } catch (GitAPIException e) {
      throw new IllegalStateException(e);
    } catch (RuntimeException exc) {
      deleteDir(repoDir);
      throw exc;
    }
  }

  public GitRepository findRepo(String name) {
    return repositories.get(name);
  }

  @PreDestroy
  @Override
  public void close() {
    repositories.values().stream().forEach(GitRepository::close);
  }

  public static enum AddStatus {
    ADDED, ALREADY_EXISTS, NAME_NOT_ALLOWED, COULD_NOT_CLONE
  }
}
