package de.gitvisualizer.server.controller;

import de.gitvisualizer.api.NewRepository;
import de.gitvisualizer.api.RepositoryInfoResponse;
import de.gitvisualizer.api.repository.LogicalRepository;
import de.gitvisualizer.server.GitRepository;
import de.gitvisualizer.server.RepositoryService;
import de.gitvisualizer.server.RepositoryService.AddStatus;
import de.gitvisualizer.server.RequestError;
import de.gitvisualizer.server.ReturnStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class APIController {

  @Autowired
  private RepositoryService repositoryService;

  @Autowired
  private LogicalRepositoryMockGenerator mockGenerator;

  @RequestMapping(method = RequestMethod.GET, value = "/repos")
  public List<RepositoryInfoResponse> repos() {
    Collection<GitRepository> repositories = repositoryService.listAll();
    return repositories.stream().map(r -> mapToRepositoriyInfoResponse(r)).collect(Collectors.toList());
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/repos/{name}/")
  public ResponseEntity<?> newRepo(@PathVariable("name") String name, @RequestBody NewRepository repo) {

    ReturnStatus<AddStatus, GitRepository> addStatus = repositoryService.addRepo(name, repo);

    if (addStatus.getStatus() == AddStatus.ALREADY_EXISTS) {
      return new ResponseEntity<RepositoryInfoResponse>(HttpStatus.CONFLICT);
    }

    if (addStatus.getStatus() == AddStatus.ADDED) {
      return new ResponseEntity<RepositoryInfoResponse>(mapToRepositoriyInfoResponse(addStatus.getResult()),
          HttpStatus.CREATED);
    }

    return new ResponseEntity<RequestError>(new RequestError("Could not create repo: " + addStatus.getStatus()),
        HttpStatus.BAD_REQUEST);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/repos/{name}/")
  public ResponseEntity<RepositoryInfoResponse> readRepo(@PathVariable("name") String name) {

    GitRepository gitRepository = repositoryService.findRepo(name);
    if (gitRepository == null) {
      return new ResponseEntity<RepositoryInfoResponse>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<RepositoryInfoResponse>(mapToRepositoriyInfoResponse(gitRepository), HttpStatus.FOUND);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/repos/{name}/logical/")
  public ResponseEntity<LogicalRepository> readRepoLogical(@PathVariable("name") String name) {

    GitRepository gitRepository = repositoryService.findRepo(name);
    if (gitRepository == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(mockGenerator.generateMock(), HttpStatus.FOUND);
  }

  private RepositoryInfoResponse mapToRepositoriyInfoResponse(GitRepository r) {
    return new RepositoryInfoResponse(r.getName(), r.getUrl(), r.getAnalyzeConf());
  }

}
