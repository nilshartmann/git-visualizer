package de.gitvisualizer.server.controller;

import de.gitvisualizer.api.repository.*;
import de.gitvisualizer.api.repository.items.BranchStart;
import de.gitvisualizer.api.repository.items.SingleCommit;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class LogicalRepositoryMockGenerator {

  private int idCounter = 0;

  public LogicalRepository generateMock() {

    // master: X-Y-Z---------H>ZZ
    // release: C>D-F-H
    // develop: A-[B1,B2,B3]-C

    BranchStart masterBranchStart = new BranchStart(newId(), "master");
    BranchStart developBranchStart = new BranchStart(newId(), "develop");

    SingleCommit v1Commit = newSingleCommit();
    SingleCommit v2Commit = newSingleCommit();
    SingleCommit v3Commit = newSingleCommit();

    LogicalBranch masterBranch = new LogicalBranch("master", asList(masterBranchStart, v1Commit, v2Commit, v3Commit));
    LogicalBranch developBranch = new LogicalBranch("develop", asList(developBranchStart, newSingleCommit(),
                                                                      newSingleCommit(), newSingleCommit()));

    BranchStart releaseBranchStart = new BranchStart(newId(), "release/v4");
    SingleCommit releaseCommit = newSingleCommit();
    LogicalBranch releaseBranch = new LogicalBranch("release/v4", asList(releaseBranchStart, releaseCommit));

    BranchItemConnection connection = new BranchItemConnection("merge", v3Commit.getId(), releaseCommit.getId());

    return new LogicalRepository(newBranches(masterBranch, developBranch, releaseBranch),
                                 newConnections(connection));
  }

  private List<BranchItemConnection> newConnections(BranchItemConnection... connections) {
    List<BranchItemConnection> result = new LinkedList<>();

    Collections.addAll(result, connections);

    return result;
  }

  private List<LogicalBranch> newBranches(LogicalBranch... branches) {
    List<LogicalBranch> result = new LinkedList<>();

    Collections.addAll(result, branches);

    return result;
  }

  private List<BranchItem> asList(BranchItem... items) {
    List<BranchItem> result = new LinkedList<>();

    Collections.addAll(result, items);

    return result;
  }

  private SingleCommit newSingleCommit() {
    String sha = "commit-" + newId();
    return new SingleCommit(newId(), newCommitDescription(sha), Collections.emptyList());
  }

  private CommitDescription newCommitDescription(final String sha) {
    return new CommitDescription(sha, "Commit " + sha, "Commit " + sha
        + "\n\nA very long commit message", Collections.emptyList());
  }

  private String newId() {
    return "item-" + idCounter++;
  }
}
