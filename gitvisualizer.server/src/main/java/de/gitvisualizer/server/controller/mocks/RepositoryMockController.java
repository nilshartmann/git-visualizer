package de.gitvisualizer.server.controller.mocks;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;

import de.gitvisualizer.api.repository.LogicalRepository;

@RestController
public class RepositoryMockController {
  private final Map<String, Resource> jsonMockFiles = new HashMap<>();
  private final Map<String, Resource> txtMockFiles = new HashMap<>();

  public RepositoryMockController() {
    try {
      Resource[] jsonResources = new PathMatchingResourcePatternResolver()
          .getResources("classpath:repository-mocks/in/*.json");
      Resource[] textResources = new PathMatchingResourcePatternResolver()
          .getResources("classpath:repository-mocks/in/*.txt");

      for (Resource resource : jsonResources) {
        String name = getShortName(resource);
        jsonMockFiles.put(name, resource);
      }
      for (Resource resource : textResources) {
        String name = getShortName(resource);
        txtMockFiles.put(name, resource);
      }
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  private String getShortName(Resource resource) throws IOException {
    URI uri = resource.getURI();
    String path = uri.toString();
    Preconditions.checkArgument(path != null);
    int index = path.lastIndexOf('/');
    Preconditions.checkArgument(index >= 0);
    String filename = path.substring(index + 1);
    String[] parts = filename.split("\\.");
    return parts[0];
  }

  @RequestMapping(method = RequestMethod.GET, value = "/mocks")
  public List<String> getAllMocks() {
    List<String> result = new ArrayList<>();
    result.addAll(jsonMockFiles.keySet());
    result.addAll(txtMockFiles.keySet());
    return result;

  }

  @RequestMapping(method = RequestMethod.GET, value = "/mocks/{name}")
  public Object getMock(@PathVariable("name") String name) throws Exception {

    Resource jsonResource = jsonMockFiles.get(name);
    if (jsonResource != null) {
      try (InputStream inputStream = jsonResource.getInputStream()) {
        String json = StreamUtils.copyToString(inputStream, Charsets.UTF_8);
        return json;
      }
    }

    Resource txtResource = txtMockFiles.get(name);
    if (txtResource != null) {
      try (InputStream inputStream = txtResource.getInputStream()) {
        String txt = StreamUtils.copyToString(inputStream, Charsets.UTF_8);
        LogicalRepository repository = LogicalRepositoryDescriptionReader.parseDescriptionFromString(txt);
        return repository;
      }
    }

    return null;
  }

}
