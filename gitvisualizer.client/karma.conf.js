module.exports = function (config) {
	config.set({

		basePath: '.',

		frameworks: ['jasmine-jquery', 'jasmine'],

		preprocessors: {
			'src/*.js': ['sourcemap']
		},

		files: [
			'src/**/*.js',
			'test/*.js',

			// fixtures
			{pattern: 'test/fixtures/*.json', watched: true, served: true, included: false}
		],

		reporters: ['progress'],

		port: 9876,

		colors: true,

		logLevel: config.LOG_DEBUG,

		autoWatch: true,

		browsers: ['ChromeCanary'],

		captureTimeout: 60000,

		singleRun: true
	});
};