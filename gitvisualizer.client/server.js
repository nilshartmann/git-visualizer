// server.js (Express 4.0)
var express        = require('express');
var app            = express();

app.use(express.static(__dirname)); 	// set the static files location /public/img will be /img for users

app.listen(12345);
console.log('Magic happens on port 12345 => ' + __dirname); 			// shoutout to the user
