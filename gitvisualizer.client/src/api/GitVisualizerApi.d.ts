/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/

declare module gitvisualizer {
	export module api {

		export interface RepositoryItem {
			type: string;
		}

		export interface LogicalRepository {
			logicalBranches: LogicalBranch[]
			branchItemConnections: BranchItemConnection[];
		}

		export interface BranchItemConnection extends RepositoryItem {
			connectionType: string;
			fromBranchItemId: string;
			toBranchItemId: string;
		}

		export interface CommitDescription {
			sha: string;
			shortMessage: string;
			fullMessage: string;
		}

		export interface BranchItem extends RepositoryItem {
			id: string;

		}

		export interface SingleCommitItem extends BranchItem {
			commitDescription: CommitDescription;
		}

		export interface CombinedCommitItem extends BranchItem {
			commitDescriptions: CommitDescription[];
		}

		export interface BranchStartItem extends BranchItem {
			name: string;
		}

		export interface LogicalBranch extends RepositoryItem {
			name: string;
			branchItems: BranchItem[];
		}
	}
}