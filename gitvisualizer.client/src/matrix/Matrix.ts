/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/
/// <reference path="./../api/GitVisualizerApi.d.ts" />
module gitvisualizer.matrix {
	import api = gitvisualizer.api;

	export class List<T> {
		private items: T[] = [];

		add(item: T): void {
			this.items.push(item);
		}

		get(index: number): T {
			return this.items[index];
		}

		asArray(): Array<T> {
			return this.items;
		}

		reverseForEach(callback: (callback: T)=>void): void {
			for (var j = this.items.length - 1; j >= 0; j--) {
				callback(this.items[j]);
			}
		}

		forEach(callback: (callback: T)=>void): void {
			this.items.forEach(callback);
		}

		transform(callback: (value: T)=>T): void {
			this.items = this.items.map((value, index, arr) => {
				var newValue = callback(value);
				return newValue;
			});
		}

		size(): number {
			return this.items.length;
		}
	}

	export class Cell {
		private next: Cell;
		private connectedTo: Cell[] = [];
		id: String;

		constructor(private prev: Cell, public item: any, private row: Row, public position: number) {
			if (item.id) {
				this.id = item.id;
			} else {
				this.id = "unkown";
			}
			if (this.prev) {
				this.prev.next = this;
			}
		}

		moveTo(newPosition: number) {
			var delta = newPosition - this.position;

			var cell = this;
			while (cell != null) {
				cell.position = cell.position + delta;

				var leftMostConnectedCell: Cell = cell.getLeftMostConnectedCell();
				if (leftMostConnectedCell && leftMostConnectedCell.position <= cell.position) {
					leftMostConnectedCell.moveTo(cell.position + 1);
				}

				cell = cell.next;
			}
		}

		getLeftMostConnectedCell() {
			var leftMostCell: Cell;
			this.connectedTo.forEach((cell)=> {
				if (leftMostCell == null || cell.position < leftMostCell.position) {
					leftMostCell = cell;
				}
			});
			return leftMostCell;
		}

		move(delta: number): Cell[] {
			var cell = this;
			var cellsToMove: Cell[] = [];
			while (cell != null) {
				cell.position = cell.position + delta;

				cell.connectedTo.forEach((connectedCell)=> {
					if (connectedCell.position < cell.position) {
						cellsToMove.push(connectedCell);
					}
				});

				cell = cell.next;
			}

			return cellsToMove;
		}

		moveRight(): boolean {
			if (this.connectedTo.length === 0) {
				return false;
			}

			var leftMostPosition = Number.MAX_VALUE;
			this.connectedTo.forEach((cell)=> {
				leftMostPosition = Math.min(leftMostPosition, cell.position);
				if (cell.position < leftMostPosition) {
					leftMostPosition = cell.position;
				}
			});

			var newPosition = leftMostPosition - 1;

			console.log("this.position: " + this.position + ", newPosition: " + newPosition);

			if (this.position > newPosition) {
				return false;
			}

			this.moveTo(newPosition);
			return true;

		}

		addConnection(to: Cell) {
			this.connectedTo.push(to);
		}
	}

	export class Row extends List<Cell> {
		constructor(private
			            _rowNumber: number) {
			super();
		}

		rowNumber() {
			return this._rowNumber;
		}
	}

	export class Matrix<C> {
		constructor(public
			            rows: Array<Row>) {
		}

		mapRow<U>(r: number, callback: (col: Cell)=>U): U[] {
			var row = this.rows[r];
			var x: U[] = [];
			row.forEach((value) => {
				x.push(callback(value));
			});
			return x;
		}
	}

	export
	interface
	RepositoryMatrixVisitor {
		visitBranchRow(branchRow: BranchRow): void;
		visitBranchCol(branchItemCell: BranchItemCell): void;
		visitConnection(branchItemConnection: api.BranchItemConnection);
	}

	export class RepositoryMatrix extends Matrix<api.BranchItem> {
		constructor(rows: Array<Row>, private branchItemConnections: api.BranchItemConnection[]) {
			super(rows);
		}

		accept(visitor: RepositoryMatrixVisitor, context?: any) {
			if (arguments.length < 2) {
				context = visitor;
			}
			for (var j = 0; j < this.rows.length; j++) {
				var row = this.rows[j];
				visitor.visitBranchRow.call(context, <BranchRow>row);

				row.forEach(function (col) {
					visitor.visitBranchCol.call(context, col);
				});
			}

			this.branchItemConnections.forEach(function (connection) {
				visitor.visitConnection.call(context, connection);
			});
		}
	}

	export class BranchRow extends Row {
		constructor(public logicalBranch: api.LogicalBranch, rowNumber: number) {
			super(rowNumber);
		}
	}
	//
	export class BranchItemCell extends Cell {
		constructor(prev: Cell, item: api.BranchItem, row: BranchRow, position: number) {
			super(prev, item, row, position);
		}
	}

	export interface Appender {
		(msg: any): void;
	}

	class Logger {
		private appender: Appender;

		log(s: any) {
			console.log(s);
			if (this.appender) {
				this.appender(s);
			}
		}

		addAppender(a: Appender) {
			this.appender = a;
		}
	}

	var LOGGER: Logger = new Logger();

	export function addAppender(a: Appender) {
		LOGGER.addAppender(a);
	}

	export class MatrixBuilder {
		constructor(private model: api.LogicalRepository, private enableOptimizer: boolean = true) {
		}

		build(): RepositoryMatrix {
			var j = 0;
			var rows: List<BranchRow> = new List<BranchRow>();
			var items: { [s: string]: Cell; } = {};

			this.model.logicalBranches.forEach((branch) => {
				var row = new BranchRow(branch, rows.size());
				var prev: Cell;
				branch.branchItems.forEach((item) => {
					var col = new Cell(prev, item, row, row.size());
					items[item.id] = col;
					row.add(col);
					prev = col;

				});
				rows.add(row);
			});

			var cs = this.model.branchItemConnections;
			var l = cs.length;

			var mergeCommits: Cell[] = [];

			this.model.branchItemConnections.forEach((connection)=> {
				var fromItem: Cell = items[connection.fromBranchItemId];
				mergeCommits.push(fromItem);
				var fromPosition = fromItem.position;
				var toItem = items[connection.toBranchItemId];
				fromItem.addConnection(toItem);
				if (fromPosition >= toItem.position) {
					// make sure, connection points "forward"
					LOGGER.log("Moving " + toItem.id + " from position " + toItem.position + " to position " + (fromPosition + 1) + " behind " + fromItem.id);
					toItem.moveTo(fromPosition + 1);
				}
			});

			if (this.enableOptimizer) {

				while (mergeCommits.length > 0) {
					console.log("MergeCommits: " + mergeCommits.length);
					var leftMostMergeCommitIx = 0;
					var leftMostMergeCommit: Cell = null;
					for (j = 0; j < mergeCommits.length; j++) {
						if (leftMostMergeCommit == null || mergeCommits[j].position < leftMostMergeCommit.position) {
							leftMostMergeCommit = mergeCommits[j];
							leftMostMergeCommitIx = j;
						}
					}

					//console.log("  Leftmost Merge Commit: Ix: " + leftMostMergeCommitIx + " position: " + leftMostMergeCommit.position);
					mergeCommits.splice(leftMostMergeCommitIx, 1);

					var expectedPosition = leftMostMergeCommit.getLeftMostConnectedCell().position - 1;
					if (leftMostMergeCommit.position < expectedPosition) {
						var delta = expectedPosition - leftMostMergeCommit.position;
						LOGGER.log("Schiebe " + leftMostMergeCommit.id + " von Position " + leftMostMergeCommit.position + " um " + delta + " positionen auf " + expectedPosition + " um eine Position hinter " + leftMostMergeCommit.getLeftMostConnectedCell().position + " zu sein")
						leftMostMergeCommit.moveTo(expectedPosition);
					}
				}
			}

			//var changed: boolean = true;
			//var xx = 0;
			//
			//while (changed===true && xx < 0) {
			//	xx++;
			//	changed = false;
			//	rows.forEach((row)=> {
			//		row.reverseForEach((cell)=> {
			//			if (cell.moveRight()) {
			//				changed = true;
			//			}
			//		});
			//	});
			//}

			return new RepositoryMatrix(rows.asArray(), this.model.branchItemConnections);

		}
	}
}

