var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/
/// <reference path="./../api/GitVisualizerApi.d.ts" />
var gitvisualizer;
(function (gitvisualizer) {
    var matrix;
    (function (matrix) {
        var List = (function () {
            function List() {
                this.items = [];
            }
            List.prototype.add = function (item) {
                this.items.push(item);
            };
            List.prototype.get = function (index) {
                return this.items[index];
            };
            List.prototype.asArray = function () {
                return this.items;
            };
            List.prototype.reverseForEach = function (callback) {
                for (var j = this.items.length - 1; j >= 0; j--) {
                    callback(this.items[j]);
                }
            };
            List.prototype.forEach = function (callback) {
                this.items.forEach(callback);
            };
            List.prototype.transform = function (callback) {
                this.items = this.items.map(function (value, index, arr) {
                    var newValue = callback(value);
                    return newValue;
                });
            };
            List.prototype.size = function () {
                return this.items.length;
            };
            return List;
        })();
        matrix.List = List;
        var Cell = (function () {
            function Cell(prev, item, row, position) {
                this.prev = prev;
                this.item = item;
                this.row = row;
                this.position = position;
                this.connectedTo = [];
                if (item.id) {
                    this.id = item.id;
                }
                else {
                    this.id = "unkown";
                }
                if (this.prev) {
                    this.prev.next = this;
                }
            }
            Cell.prototype.moveTo = function (newPosition) {
                var delta = newPosition - this.position;
                var cell = this;
                while (cell != null) {
                    cell.position = cell.position + delta;
                    var leftMostConnectedCell = cell.getLeftMostConnectedCell();
                    if (leftMostConnectedCell && leftMostConnectedCell.position <= cell.position) {
                        leftMostConnectedCell.moveTo(cell.position + 1);
                    }
                    cell = cell.next;
                }
            };
            Cell.prototype.getLeftMostConnectedCell = function () {
                var leftMostCell;
                this.connectedTo.forEach(function (cell) {
                    if (leftMostCell == null || cell.position < leftMostCell.position) {
                        leftMostCell = cell;
                    }
                });
                return leftMostCell;
            };
            Cell.prototype.move = function (delta) {
                var cell = this;
                var cellsToMove = [];
                while (cell != null) {
                    cell.position = cell.position + delta;
                    cell.connectedTo.forEach(function (connectedCell) {
                        if (connectedCell.position < cell.position) {
                            cellsToMove.push(connectedCell);
                        }
                    });
                    cell = cell.next;
                }
                return cellsToMove;
            };
            Cell.prototype.moveRight = function () {
                if (this.connectedTo.length === 0) {
                    return false;
                }
                var leftMostPosition = Number.MAX_VALUE;
                this.connectedTo.forEach(function (cell) {
                    leftMostPosition = Math.min(leftMostPosition, cell.position);
                    if (cell.position < leftMostPosition) {
                        leftMostPosition = cell.position;
                    }
                });
                var newPosition = leftMostPosition - 1;
                console.log("this.position: " + this.position + ", newPosition: " + newPosition);
                if (this.position > newPosition) {
                    return false;
                }
                this.moveTo(newPosition);
                return true;
            };
            Cell.prototype.addConnection = function (to) {
                this.connectedTo.push(to);
            };
            return Cell;
        })();
        matrix.Cell = Cell;
        var Row = (function (_super) {
            __extends(Row, _super);
            function Row(_rowNumber) {
                _super.call(this);
                this._rowNumber = _rowNumber;
            }
            Row.prototype.rowNumber = function () {
                return this._rowNumber;
            };
            return Row;
        })(List);
        matrix.Row = Row;
        var Matrix = (function () {
            function Matrix(rows) {
                this.rows = rows;
            }
            Matrix.prototype.mapRow = function (r, callback) {
                var row = this.rows[r];
                var x = [];
                row.forEach(function (value) {
                    x.push(callback(value));
                });
                return x;
            };
            return Matrix;
        })();
        matrix.Matrix = Matrix;
        var RepositoryMatrix = (function (_super) {
            __extends(RepositoryMatrix, _super);
            function RepositoryMatrix(rows, branchItemConnections) {
                _super.call(this, rows);
                this.branchItemConnections = branchItemConnections;
            }
            RepositoryMatrix.prototype.accept = function (visitor, context) {
                if (arguments.length < 2) {
                    context = visitor;
                }
                for (var j = 0; j < this.rows.length; j++) {
                    var row = this.rows[j];
                    visitor.visitBranchRow.call(context, row);
                    row.forEach(function (col) {
                        visitor.visitBranchCol.call(context, col);
                    });
                }
                this.branchItemConnections.forEach(function (connection) {
                    visitor.visitConnection.call(context, connection);
                });
            };
            return RepositoryMatrix;
        })(Matrix);
        matrix.RepositoryMatrix = RepositoryMatrix;
        var BranchRow = (function (_super) {
            __extends(BranchRow, _super);
            function BranchRow(logicalBranch, rowNumber) {
                _super.call(this, rowNumber);
                this.logicalBranch = logicalBranch;
            }
            return BranchRow;
        })(Row);
        matrix.BranchRow = BranchRow;
        //
        var BranchItemCell = (function (_super) {
            __extends(BranchItemCell, _super);
            function BranchItemCell(prev, item, row, position) {
                _super.call(this, prev, item, row, position);
            }
            return BranchItemCell;
        })(Cell);
        matrix.BranchItemCell = BranchItemCell;
        var Logger = (function () {
            function Logger() {
            }
            Logger.prototype.log = function (s) {
                console.log(s);
                if (this.appender) {
                    this.appender(s);
                }
            };
            Logger.prototype.addAppender = function (a) {
                this.appender = a;
            };
            return Logger;
        })();
        var LOGGER = new Logger();
        function addAppender(a) {
            LOGGER.addAppender(a);
        }
        matrix.addAppender = addAppender;
        var MatrixBuilder = (function () {
            function MatrixBuilder(model, enableOptimizer) {
                if (enableOptimizer === void 0) { enableOptimizer = true; }
                this.model = model;
                this.enableOptimizer = enableOptimizer;
            }
            MatrixBuilder.prototype.build = function () {
                var j = 0;
                var rows = new List();
                var items = {};
                this.model.logicalBranches.forEach(function (branch) {
                    var row = new BranchRow(branch, rows.size());
                    var prev;
                    branch.branchItems.forEach(function (item) {
                        var col = new Cell(prev, item, row, row.size());
                        items[item.id] = col;
                        row.add(col);
                        prev = col;
                    });
                    rows.add(row);
                });
                var cs = this.model.branchItemConnections;
                var l = cs.length;
                var mergeCommits = [];
                this.model.branchItemConnections.forEach(function (connection) {
                    var fromItem = items[connection.fromBranchItemId];
                    mergeCommits.push(fromItem);
                    var fromPosition = fromItem.position;
                    var toItem = items[connection.toBranchItemId];
                    fromItem.addConnection(toItem);
                    if (fromPosition >= toItem.position) {
                        // make sure, connection points "forward"
                        LOGGER.log("Moving " + toItem.id + " from position " + toItem.position + " to position " + (fromPosition + 1) + " behind " + fromItem.id);
                        toItem.moveTo(fromPosition + 1);
                    }
                });
                if (this.enableOptimizer) {
                    while (mergeCommits.length > 0) {
                        console.log("MergeCommits: " + mergeCommits.length);
                        var leftMostMergeCommitIx = 0;
                        var leftMostMergeCommit = null;
                        for (j = 0; j < mergeCommits.length; j++) {
                            if (leftMostMergeCommit == null || mergeCommits[j].position < leftMostMergeCommit.position) {
                                leftMostMergeCommit = mergeCommits[j];
                                leftMostMergeCommitIx = j;
                            }
                        }
                        //console.log("  Leftmost Merge Commit: Ix: " + leftMostMergeCommitIx + " position: " + leftMostMergeCommit.position);
                        mergeCommits.splice(leftMostMergeCommitIx, 1);
                        var expectedPosition = leftMostMergeCommit.getLeftMostConnectedCell().position - 1;
                        if (leftMostMergeCommit.position < expectedPosition) {
                            var delta = expectedPosition - leftMostMergeCommit.position;
                            LOGGER.log("Schiebe " + leftMostMergeCommit.id + " von Position " + leftMostMergeCommit.position + " um " + delta + " positionen auf " + expectedPosition + " um eine Position hinter " + leftMostMergeCommit.getLeftMostConnectedCell().position + " zu sein");
                            leftMostMergeCommit.moveTo(expectedPosition);
                        }
                    }
                }
                //var changed: boolean = true;
                //var xx = 0;
                //
                //while (changed===true && xx < 0) {
                //	xx++;
                //	changed = false;
                //	rows.forEach((row)=> {
                //		row.reverseForEach((cell)=> {
                //			if (cell.moveRight()) {
                //				changed = true;
                //			}
                //		});
                //	});
                //}
                return new RepositoryMatrix(rows.asArray(), this.model.branchItemConnections);
            };
            return MatrixBuilder;
        })();
        matrix.MatrixBuilder = MatrixBuilder;
    })(matrix = gitvisualizer.matrix || (gitvisualizer.matrix = {}));
})(gitvisualizer || (gitvisualizer = {}));
//# sourceMappingURL=Matrix.js.map