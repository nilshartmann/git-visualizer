/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/
/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="./GitVisualizerClient.ts" />
var gitvisualizer;
(function (gitvisualizer) {
    var client;
    (function (client) {
        var MainController = (function () {
            function MainController($scope) {
                console.log("MAIN CONTROLLER!");
            }
            return MainController;
        })();
        client.MainController = MainController;
        angular.module(gitvisualizer.client.GitVisualizerClientModule).controller('gitvisualizer.client.mainController', gitvisualizer.client.MainController);
    })(client = gitvisualizer.client || (gitvisualizer.client = {}));
})(gitvisualizer || (gitvisualizer = {}));
//# sourceMappingURL=MainController.js.map