/// <reference path="../../typings/raphael/raphael.d.ts" />
/// <reference path="../matrix/Matrix.ts" />
module gitvisualizer.ui {

	import RepositoryMatrix = gitvisualizer.matrix.RepositoryMatrix;
	import BranchItemCell = gitvisualizer.matrix.BranchItemCell;
	import RepositoryItem = gitvisualizer.api.RepositoryItem;
	import LogicalBranch = gitvisualizer.api.LogicalBranch;
	import BranchItemConnection = gitvisualizer.api.BranchItemConnection;
	import BranchItem = gitvisualizer.api.BranchItem;

	export interface Sizes {
		/** Height of Branch lanes */
		laneHeight: number;
		/** Height of label */
		laneLabelHeight: number;
		/** Distance between two branch lanes */
		laneDistance: number;
		/** Indent of a branch lane (left and right) */
		laneIndent: number;
		/** Horizontal space between two items on a branch lane */
		laneItemDistance: number;
		/** Height of Items on a branch lane */
		elementHeight: number;
	}

	/** Some default Sizes */
	var DEFAULT_SIZES: Sizes = {
		laneHeight:       60,
		laneLabelHeight:  20,
		laneDistance:     45,
		laneIndent:       45,
		laneItemDistance: 45,
		elementHeight:    30
	};

	/** Represents a pair of foreground and background color */
	class ColorPair {
		constructor(private fgColor: string, private bgColor: string, private textColor: string) {
		}

		get fg() {
			return this.fgColor;
		}

		get bg() {
			return this.bgColor;
		}

		get text() {
			return this.textColor;
		}
	}

	export class ColorSchemes {
		public static INDIGO = [
			new ColorPair("#8C9EFF", "#C5CAE9", "rgb(242, 248, 251)"),
			//new ColorPair("#8C9EFF","#9FA8DA", "rgb(242, 248, 251)"),
			new ColorPair("#8C9EFF", "#7986CB", "rgb(242, 248, 251)"),
			new ColorPair("#8C9EFF", "#5C6BC0", "rgb(242, 248, 251)"),
			//new ColorPair("#8C9EFF","#3F51B5", "rgb(242, 248, 251)"),
			new ColorPair("#8C9EFF", "#3949AB", "rgb(242, 248, 251)"),
			//new ColorPair("#8C9EFF","#303F9F", "rgb(242, 248, 251)"),
			//new ColorPair("#8C9EFF","#283593", "rgb(242, 248, 251)"),
			new ColorPair("#8C9EFF", "#1A237E", "rgb(242, 248, 251)")
		]
	}

	/** TODO: Configurable by users */
	class UiConfig {
		private static DEFAULT_COLORS = [
			/* Taken from Material Design Color Palette http://www.google.com/design/spec/style/color.html#color-color-palette */
			new ColorPair("#F44336", "#FFCDD2", "#F44336"), // RED
			new ColorPair("#FF9800", "#FFE0B2", "#FF9800"), // Orange
			new ColorPair("#FF5722", "#FFCCBC", "#FF5722"), // Deep Orange
			new ColorPair("#E91E63", "#F8BBD0", "#E91E63"), // Pink
			new ColorPair("#9C27B0", "#E1BEE7", "#9C27B0"), // Purple
			new ColorPair("#673AB7", "#D1C4E9", "#673AB7"), // Deep Purple
			new ColorPair("#3F51B5", "#C5CAE9", "#3F51B5"), // Indigo
			new ColorPair("#2196F3", "#BBDEFB", "#2196F3"), // Blue
			new ColorPair("#00BCD4", "#B2EBF2", "#00BCD4"), // Cyan
			new ColorPair("#009688", "#B2DFDB", "#009688"), // Tael
			new ColorPair("#4CAF50", "#C8E6C9", "#4CAF50"), // Green
			new ColorPair("#8BC34A", "#DCEDC8", "#8BC34A"), // Light Green
			new ColorPair("#CDDC39", "#F0F4C3", "#CDDC39"), // Lime
			new ColorPair("#FFEB3B", "#FFF9C4", "#FFEB3B"), // Yellow
			new ColorPair("#FFC107", "#FFECB3", "#FFC107"), // Amber
			new ColorPair("#795548", "#D7CCC8", "#795548"), // Brown
			new ColorPair("#9E9E9E", "#F5F5F5", "#9E9E9E"), // Grey
			new ColorPair("#607D8B", "#CFD8DC", "#607D8B"), // Blue Grey

			// Indigo
			//new ColorPair("rgba(0,0,0,0.87)","#C5CAE9"),
			//new ColorPair("rgba(0,0,0,0.87)","#9FA8DA"),
			//new ColorPair("#FFF","#7986CB"),
			//new ColorPair("#FFF","#5C6BC0"),
			//new ColorPair("#FFF","#3F51B5"),
			//new ColorPair("#FFF","#3949AB"),
			//new ColorPair("#FFF","#303F9F"),
			//new ColorPair("#FFF","#283593"),
			//new ColorPair("#FFF","#1A237E")
		];

		private colorScheme: ColorPair[] = UiConfig.DEFAULT_COLORS;

		/** Return the color scheme for the given lane */
		colorForLane(laneNumber: number) {
			var colorNumber = laneNumber % this.colorScheme.length;
			return this.colorScheme[colorNumber];
		}

		setColorScheme(colors: ColorPair[]) {
			this.colorScheme = colors;
		}
	}

	class RepositoryElement<T extends api.RepositoryItem> {
		constructor(private _repositoryItem: T, private _sizes: Sizes) {
		}

		/** return the api.BranchItem that is represented by this BranchElement */
		get repositoryItem(): T {
			return this._repositoryItem;
		}

		get sizes(): Sizes {
			return this._sizes;
		}

		registerMouseListener(element: RaphaelElement, listener: RepositoryElementSelectedEventListener) {
			if (!listener) {
				// no one cares
				return;
			}

			element.click(() => {
				listener.repositoryElementSelected({
					repositoryElement: this,
					eventType:         RepositoryElementSelectedEventType.Clicked
				});
			});
			element.mouseover(() => {
				listener.repositoryElementSelected({
					repositoryElement: this,
					eventType:         RepositoryElementSelectedEventType.Over
				});
			});
			element.mouseout(() => {
				listener.repositoryElementSelected({
					repositoryElement: this,
					eventType:         RepositoryElementSelectedEventType.Out
				});
			});
		}
	}

	/* abstract */
	class BranchElement<T extends api.BranchItem> extends RepositoryElement<T> {
		constructor(private _branchLane: BranchLane, sizes: Sizes, private branchItemCol: BranchItemCell, private _position: Position, public bgColor: string) {
			super(<T>branchItemCol.item, sizes);
		}

		draw(paper: RaphaelPaper, listener?: RepositoryElementSelectedEventListener): void {
			throw new Error("Invoking abstract method. Must override in subclasses");
		}

		/** Returns the absolute position of this element in pixels */
		get position(): Position {
			return this._position;
		}

		get branchLane(): BranchLane {
			return this._branchLane;
		}
	}

	class BranchStartElement extends BranchElement<api.BranchStartItem> {
		constructor(lane: BranchLane, sizes: Sizes, branchItemCol: BranchItemCell, position: Position, bgColor: string) {
			super(lane, sizes, branchItemCol, position, bgColor);
		}

		draw(paper: RaphaelPaper, listener?: RepositoryElementSelectedEventListener): void {
			var height = this.sizes.elementHeight;
			var y = this.position.y - (height / 2);
			var command = "M" + this.position.x + "," + y //
					+ "L" + this.position.x + "," + (y + height)
					+ "L" + (this.position.x + height) + "," + this.position.y
					+ "C"
				;
			var path = paper.path(command);
			path.attr("stroke", this.bgColor);
			path.attr("fill", this.bgColor);
			path.attr("stroke-width", 1);

			this.registerMouseListener(path, listener);
		}
	}

	class SingleCommitElement extends BranchElement<api.SingleCommitItem> {
		private radius: number;

		constructor(lane: BranchLane, sizes: Sizes, branchItemCol: BranchItemCell, position: Position, bgColor: string) {
			super(lane, sizes, branchItemCol, position, bgColor);
			this.radius = sizes.elementHeight / 2;
		}

		draw(paper: RaphaelPaper, listener?: RepositoryElementSelectedEventListener): void {
			var circle = paper.circle(this.position.x, this.position.y, this.radius);
			circle.attr("fill", this.bgColor);
			circle.attr("stroke", this.bgColor);

			this.registerMouseListener(circle, listener);
		}
	}

	class CombinedCommitElement extends BranchElement<api.CombinedCommitItem> {
		private radius: number;
		private innerColor: string;

		constructor(lane: BranchLane, sizes: Sizes, branchItemCol: BranchItemCell, position: Position, bgColor: ColorPair) {
			super(lane, sizes, branchItemCol, position, bgColor.fg);
			this.radius = sizes.elementHeight / 2;
			this.innerColor = bgColor.bg;
		}

		draw(paper: RaphaelPaper, listener?: RepositoryElementSelectedEventListener): void {
			var circle = paper.circle(this.position.x, this.position.y, this.radius);
			circle.attr("fill", this.bgColor);
			circle.attr("stroke", this.bgColor);

			// draw "small dots"
			var miniRadius = (this.radius / 5);
			var leftMiniCircle = this.position.x - (3 * miniRadius);
			for (var i = 0; i < 3; i++) {
				var miniCircle = paper.circle(leftMiniCircle + (i * miniRadius * 3), this.position.y, miniRadius);
				miniCircle.attr("fill", this.innerColor);
				miniCircle.attr("stroke", this.innerColor);
			}

			this.registerMouseListener(circle, listener);
		}
	}

	class Position {
		private _x: number;
		private _y: number;

		constructor(x: number, y: number) {
			this._x = x;
			this._y = y;
		}

		get x() {
			return this._x;
		}

		get y() {
			return this._y;
		}
	}

	/** A "lane" representing a branch
	 *
	 */
	class BranchLane extends RepositoryElement<api.LogicalBranch> {
		/** element representing this lane */
		private rect: RaphaelElement;

		/** y-position of this lane */
		y: number = 0;

		/** Overall width of the whole branch lane */
		laneWidth: number = 0;

		/** overall height of this lane */
		laneHeight: number;

		/** indent left and right */
		indent: number;

		/** distance between two items on this lane (TODO: middle-to-middle or left-to-left???) */
		itemDistance: number;
		/** x-position of Leftmost item */
		private left = 9999;

		/** x-position of rightmost item */
		private right = 0;

		constructor(private count: number, sizes: Sizes, logicalBranch: LogicalBranch, public color: ColorPair) {
			super(logicalBranch, sizes);
			this.indent = sizes.laneIndent;
			this.laneHeight = sizes.laneHeight;
			this.itemDistance = sizes.laneItemDistance;
			this.y = (this.count) * (this.laneHeight + sizes.laneDistance);
		}

		draw(paper: RaphaelPaper, listener?: RepositoryElementSelectedEventListener): void {
			this.rect = paper.rect(0, this.y, this.laneWidth, this.laneHeight);
			this.rect.attr("fill", this.color.bg);
			this.rect.attr("stroke", this.color.bg);

			this.drawBranchLine(paper, listener);
			this.laneWidth += this.drawBranchTitle(paper);
		}

		resize(newWidth: number) {
			this.rect.attr("width", newWidth);
		}

		private drawBranchTitle(paper: RaphaelPaper): number {
			var textHeight = this.sizes.laneLabelHeight;
			var y = this.y + (this.laneHeight / 2);
			var t = paper.text(this.laneWidth + this.indent, y, this.repositoryItem.name);
			t.attr({"font-size": textHeight, "font-family": "RobotoDraft"});
			t.attr("fill", this.color.text);
			return t.node['textLength'].baseVal.value;
		}

		private drawBranchLine(paper: RaphaelPaper, listener?: RepositoryElementSelectedEventListener): void {
			var height = this.sizes.elementHeight / 4;
			var y = this.y + (this.laneHeight / 2);

			var command = "M" + this.left + "," + y + "L" + this.right + "," + y;
			var path = paper.path(command);
			path.attr("stroke", this.color.fg);
			path.attr("stroke-width", height);

			if (listener) {
				this.registerMouseListener(path, listener);
			}
		}

		get laneNumber(): number {
			return this.count;
		}

		/**
		 * Get coordinates of the given column number.
		 *
		 * The x-coordinate points points to the left most position of the new item
		 * The y-coordinate points to the middle of the new item (i.e. middle of the lane)
		 */
		getPosition(col: number): Position {
			var x = this.indent + ((this.itemDistance + 1 + this.sizes.laneHeight) * col);
			var y = this.y + (this.laneHeight / 2);

			// Update leftmost and rightmost positions and overall width
			this.left = Math.min(this.left, x);
			this.right = Math.max(this.right, x);
			this.laneWidth = Math.max(this.laneWidth, x + this.sizes.elementHeight + this.indent);

			return new Position(x, y);
		}
	}

	/** A Connection between to branch elements */
	class Connection extends RepositoryElement<BranchItemConnection> {
		private lineHeight: number;

		constructor(branchItemConnection: BranchItemConnection, sizes: Sizes, public from: BranchElement<any>, public to: BranchElement<any>) {
			super(branchItemConnection, sizes);
			this.lineHeight = sizes.elementHeight / 4
		}

		draw(paper: RaphaelPaper, listener?: RepositoryElementSelectedEventListener) {
			var command = "M" + this.from.position.x + "," + this.from.position.y + "L" + this.to.position.x + "," + this.to.position.y;
			var path = paper.path(command);

			var color;
			if (this.repositoryItem.connectionType == 'branchStart') {
				color = this.to.bgColor;
			} else {
				color = (this.from.branchLane.laneNumber > this.to.branchLane.laneNumber ? this.from.bgColor : this.to.bgColor);
			}
			path.attr("stroke", color);
			path.attr("stroke-width", this.lineHeight);
			this.registerMouseListener(path, listener);
		}
	}

	enum RepositoryElementSelectedEventType {
		Over, Out, Clicked
	}

	interface RepositoryElementSelectedEvent {
		repositoryElement: RepositoryElement<any>;
		eventType: RepositoryElementSelectedEventType;

	}

	interface RepositoryElementSelectedEventListener {
		repositoryElementSelected(event: RepositoryElementSelectedEvent): void;
	}

	// TODO use dedicated methods instead of eventType enum ?
	export enum RepositoryItemSelectedEventType {
		/** The Item has been selected (clicked with the mouse) */
		ItemSelected,
		/** The mouse is over the item */
		ItemOver,
		/** Mouse has left the item */
		ItemOut
	}

	/**
	 * Event that is fired when an item from a repository is
	 * selected or hover'd by the the mouse
	 */
	export interface RepositoryItemSelectedEvent {
		/** The affected item */
		branchItem: BranchItem;

		/** */
		eventType: RepositoryItemSelectedEventType;
	}


	export interface RepositoryItemSelectedEventListener {
		(event: RepositoryItemSelectedEvent): void;
	}

	export interface PaperCustomizer {
		(paper: RaphaelPaper):void;
	}

	export class GitHistoryGraph implements RepositoryElementSelectedEventListener {
		private uiConfig: UiConfig = new UiConfig();
		private sizes: Sizes = DEFAULT_SIZES;
		private lanes: BranchLane[] = [];
		private commits: { [s: string]: BranchElement<any>; } = {};
		private connections: Connection[] = [];
		private repositoryItemSelectedEventListener: RepositoryItemSelectedEventListener;

		constructor(private panelElementId: string, private repositoryMatrix: RepositoryMatrix) {
		}

		setup(): void {
			this.repositoryMatrix.accept({
				visitBranchRow:  function (br) {
					var branchLane = new BranchLane(this.lanes.length, this.sizes, br.logicalBranch, this.uiConfig.colorForLane(this.lanes.length));
					this.lanes.push(branchLane);
				},
				visitBranchCol: function (c: BranchItemCell) {
					var branchLane = this.lanes[this.lanes.length - 1];
					var branchElement: BranchElement<any>;
					var position: Position = branchLane.getPosition(c.position);
					if (c.item.type === 'BranchStart') {
						branchElement = new BranchStartElement(branchLane, this.sizes, c, position, branchLane.color.fg);
					} else if (c.item.type === 'CombinedCommit') {
						branchElement = new CombinedCommitElement(branchLane, this.sizes, c, position, branchLane.color);
					} else {
						branchElement = new SingleCommitElement(branchLane, this.sizes, c, position, branchLane.color.fg);
					}
					this.commits[c.item.id] = branchElement;
				},
				visitConnection: function (c: BranchItemConnection) {
					var commit1 = this.commits[c.fromBranchItemId];
					var commit2 = this.commits[c.toBranchItemId];
					var connection = new Connection(c, this.sizes, commit1, commit2);
					this.connections.push(connection);
				}
			}, this);
		}

		setColorScheme(colorScheme: ColorPair[]): void {
			this.uiConfig.setColorScheme(colorScheme);
		}

		setSizes(sizes: Sizes): void {
			this.sizes = sizes;
		}

		draw(raphaelCustomizer?: PaperCustomizer): void {
			// STEP 1: GET WIDEST LANE
			// --------------------------------------------------------------------------------------------------
			var width = 0;
			var height = 0;
			this.lanes.forEach((branchLane) => {
				width = Math.max(width, branchLane.laneWidth);
				height = Math.max(height, (branchLane.y + branchLane.laneHeight));
			});

			// STEP 1: MAKE ALL LINES SAME WIDTH
			// --------------------------------------------------------------------------------------------------
			this.lanes.forEach((branchLane) => {
				branchLane.laneWidth = width;
			});

			// STEP 2: CREATE THE 'PAPER'
			// --------------------------------------------------------------------------------------------------
			// TODO calculate width
			var paper = Raphael(this.panelElementId, width + 200, height);
			if (raphaelCustomizer) {
				raphaelCustomizer(paper);
			}

			// STEP 2-A: ADD ALL LANES
			var newWidth = 0;
			this.lanes.forEach((branchLane) => {
				branchLane.draw(paper, this);
				if (branchLane.laneWidth > newWidth) {
					newWidth = branchLane.laneWidth;
				}
			});

			// STEP 2-B: RE-AJUST THE LANE WIDTHS
			this.lanes.forEach((branchLane) => {
				branchLane.resize(newWidth);
			});

			// STEP 3: DRAW CONNECTIONS
			// --------------------------------------------------------------------------------------------------
			this.connections.forEach((connection) => {
				connection.draw(paper, this);
			});

			// STEP 4: DRAW BRANCH ITEMS
			// --------------------------------------------------------------------------------------------------
			for (var key in this.commits) {
				var commit = this.commits[key];
				commit.draw(paper, this);
			}
		}

		setRepositoryItemSelectedEventListener(listener: RepositoryItemSelectedEventListener) {
			this.repositoryItemSelectedEventListener = listener;
		}

		repositoryElementSelected(event: RepositoryElementSelectedEvent): void {
			if (!this.repositoryItemSelectedEventListener) {
				return;
			}

			var repositoryItemSelectedEventType: RepositoryItemSelectedEventType;
			switch (event.eventType) {
				case RepositoryElementSelectedEventType.Clicked:
					repositoryItemSelectedEventType = RepositoryItemSelectedEventType.ItemSelected;
					break;
				case RepositoryElementSelectedEventType.Out:
					repositoryItemSelectedEventType = RepositoryItemSelectedEventType.ItemOut;
					break;
				case RepositoryElementSelectedEventType.Over:
					repositoryItemSelectedEventType = RepositoryItemSelectedEventType.ItemOver;
					break;
			}

			this.repositoryItemSelectedEventListener({
				branchItem: event.repositoryElement.repositoryItem,
				eventType:  repositoryItemSelectedEventType
			});
		}
	}
}