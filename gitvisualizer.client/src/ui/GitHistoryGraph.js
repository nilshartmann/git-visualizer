var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="../../typings/raphael/raphael.d.ts" />
/// <reference path="../matrix/Matrix.ts" />
var gitvisualizer;
(function (gitvisualizer) {
    var ui;
    (function (ui) {
        /** Some default Sizes */
        var DEFAULT_SIZES = {
            laneHeight: 60,
            laneLabelHeight: 20,
            laneDistance: 45,
            laneIndent: 45,
            laneItemDistance: 45,
            elementHeight: 30
        };
        /** Represents a pair of foreground and background color */
        var ColorPair = (function () {
            function ColorPair(fgColor, bgColor, textColor) {
                this.fgColor = fgColor;
                this.bgColor = bgColor;
                this.textColor = textColor;
            }
            Object.defineProperty(ColorPair.prototype, "fg", {
                get: function () {
                    return this.fgColor;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorPair.prototype, "bg", {
                get: function () {
                    return this.bgColor;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorPair.prototype, "text", {
                get: function () {
                    return this.textColor;
                },
                enumerable: true,
                configurable: true
            });
            return ColorPair;
        })();
        var ColorSchemes = (function () {
            function ColorSchemes() {
            }
            ColorSchemes.INDIGO = [
                new ColorPair("#8C9EFF", "#C5CAE9", "rgb(242, 248, 251)"),
                new ColorPair("#8C9EFF", "#7986CB", "rgb(242, 248, 251)"),
                new ColorPair("#8C9EFF", "#5C6BC0", "rgb(242, 248, 251)"),
                new ColorPair("#8C9EFF", "#3949AB", "rgb(242, 248, 251)"),
                new ColorPair("#8C9EFF", "#1A237E", "rgb(242, 248, 251)")
            ];
            return ColorSchemes;
        })();
        ui.ColorSchemes = ColorSchemes;
        /** TODO: Configurable by users */
        var UiConfig = (function () {
            function UiConfig() {
                this.colorScheme = UiConfig.DEFAULT_COLORS;
            }
            /** Return the color scheme for the given lane */
            UiConfig.prototype.colorForLane = function (laneNumber) {
                var colorNumber = laneNumber % this.colorScheme.length;
                return this.colorScheme[colorNumber];
            };
            UiConfig.prototype.setColorScheme = function (colors) {
                this.colorScheme = colors;
            };
            UiConfig.DEFAULT_COLORS = [
                new ColorPair("#F44336", "#FFCDD2", "#F44336"),
                new ColorPair("#FF9800", "#FFE0B2", "#FF9800"),
                new ColorPair("#FF5722", "#FFCCBC", "#FF5722"),
                new ColorPair("#E91E63", "#F8BBD0", "#E91E63"),
                new ColorPair("#9C27B0", "#E1BEE7", "#9C27B0"),
                new ColorPair("#673AB7", "#D1C4E9", "#673AB7"),
                new ColorPair("#3F51B5", "#C5CAE9", "#3F51B5"),
                new ColorPair("#2196F3", "#BBDEFB", "#2196F3"),
                new ColorPair("#00BCD4", "#B2EBF2", "#00BCD4"),
                new ColorPair("#009688", "#B2DFDB", "#009688"),
                new ColorPair("#4CAF50", "#C8E6C9", "#4CAF50"),
                new ColorPair("#8BC34A", "#DCEDC8", "#8BC34A"),
                new ColorPair("#CDDC39", "#F0F4C3", "#CDDC39"),
                new ColorPair("#FFEB3B", "#FFF9C4", "#FFEB3B"),
                new ColorPair("#FFC107", "#FFECB3", "#FFC107"),
                new ColorPair("#795548", "#D7CCC8", "#795548"),
                new ColorPair("#9E9E9E", "#F5F5F5", "#9E9E9E"),
                new ColorPair("#607D8B", "#CFD8DC", "#607D8B"),
            ];
            return UiConfig;
        })();
        var RepositoryElement = (function () {
            function RepositoryElement(_repositoryItem, _sizes) {
                this._repositoryItem = _repositoryItem;
                this._sizes = _sizes;
            }
            Object.defineProperty(RepositoryElement.prototype, "repositoryItem", {
                /** return the api.BranchItem that is represented by this BranchElement */
                get: function () {
                    return this._repositoryItem;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(RepositoryElement.prototype, "sizes", {
                get: function () {
                    return this._sizes;
                },
                enumerable: true,
                configurable: true
            });
            RepositoryElement.prototype.registerMouseListener = function (element, listener) {
                var _this = this;
                if (!listener) {
                    // no one cares
                    return;
                }
                element.click(function () {
                    listener.repositoryElementSelected({
                        repositoryElement: _this,
                        eventType: 2 /* Clicked */
                    });
                });
                element.mouseover(function () {
                    listener.repositoryElementSelected({
                        repositoryElement: _this,
                        eventType: 0 /* Over */
                    });
                });
                element.mouseout(function () {
                    listener.repositoryElementSelected({
                        repositoryElement: _this,
                        eventType: 1 /* Out */
                    });
                });
            };
            return RepositoryElement;
        })();
        /* abstract */
        var BranchElement = (function (_super) {
            __extends(BranchElement, _super);
            function BranchElement(_branchLane, sizes, branchItemCol, _position, bgColor) {
                _super.call(this, branchItemCol.item, sizes);
                this._branchLane = _branchLane;
                this.branchItemCol = branchItemCol;
                this._position = _position;
                this.bgColor = bgColor;
            }
            BranchElement.prototype.draw = function (paper, listener) {
                throw new Error("Invoking abstract method. Must override in subclasses");
            };
            Object.defineProperty(BranchElement.prototype, "position", {
                /** Returns the absolute position of this element in pixels */
                get: function () {
                    return this._position;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BranchElement.prototype, "branchLane", {
                get: function () {
                    return this._branchLane;
                },
                enumerable: true,
                configurable: true
            });
            return BranchElement;
        })(RepositoryElement);
        var BranchStartElement = (function (_super) {
            __extends(BranchStartElement, _super);
            function BranchStartElement(lane, sizes, branchItemCol, position, bgColor) {
                _super.call(this, lane, sizes, branchItemCol, position, bgColor);
            }
            BranchStartElement.prototype.draw = function (paper, listener) {
                var height = this.sizes.elementHeight;
                var y = this.position.y - (height / 2);
                var command = "M" + this.position.x + "," + y + "L" + this.position.x + "," + (y + height) + "L" + (this.position.x + height) + "," + this.position.y + "C";
                var path = paper.path(command);
                path.attr("stroke", this.bgColor);
                path.attr("fill", this.bgColor);
                path.attr("stroke-width", 1);
                this.registerMouseListener(path, listener);
            };
            return BranchStartElement;
        })(BranchElement);
        var SingleCommitElement = (function (_super) {
            __extends(SingleCommitElement, _super);
            function SingleCommitElement(lane, sizes, branchItemCol, position, bgColor) {
                _super.call(this, lane, sizes, branchItemCol, position, bgColor);
                this.radius = sizes.elementHeight / 2;
            }
            SingleCommitElement.prototype.draw = function (paper, listener) {
                var circle = paper.circle(this.position.x, this.position.y, this.radius);
                circle.attr("fill", this.bgColor);
                circle.attr("stroke", this.bgColor);
                this.registerMouseListener(circle, listener);
            };
            return SingleCommitElement;
        })(BranchElement);
        var CombinedCommitElement = (function (_super) {
            __extends(CombinedCommitElement, _super);
            function CombinedCommitElement(lane, sizes, branchItemCol, position, bgColor) {
                _super.call(this, lane, sizes, branchItemCol, position, bgColor.fg);
                this.radius = sizes.elementHeight / 2;
                this.innerColor = bgColor.bg;
            }
            CombinedCommitElement.prototype.draw = function (paper, listener) {
                var circle = paper.circle(this.position.x, this.position.y, this.radius);
                circle.attr("fill", this.bgColor);
                circle.attr("stroke", this.bgColor);
                // draw "small dots"
                var miniRadius = (this.radius / 5);
                var leftMiniCircle = this.position.x - (3 * miniRadius);
                for (var i = 0; i < 3; i++) {
                    var miniCircle = paper.circle(leftMiniCircle + (i * miniRadius * 3), this.position.y, miniRadius);
                    miniCircle.attr("fill", this.innerColor);
                    miniCircle.attr("stroke", this.innerColor);
                }
                this.registerMouseListener(circle, listener);
            };
            return CombinedCommitElement;
        })(BranchElement);
        var Position = (function () {
            function Position(x, y) {
                this._x = x;
                this._y = y;
            }
            Object.defineProperty(Position.prototype, "x", {
                get: function () {
                    return this._x;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Position.prototype, "y", {
                get: function () {
                    return this._y;
                },
                enumerable: true,
                configurable: true
            });
            return Position;
        })();
        /** A "lane" representing a branch
         *
         */
        var BranchLane = (function (_super) {
            __extends(BranchLane, _super);
            function BranchLane(count, sizes, logicalBranch, color) {
                _super.call(this, logicalBranch, sizes);
                this.count = count;
                this.color = color;
                /** y-position of this lane */
                this.y = 0;
                /** Overall width of the whole branch lane */
                this.laneWidth = 0;
                /** x-position of Leftmost item */
                this.left = 9999;
                /** x-position of rightmost item */
                this.right = 0;
                this.indent = sizes.laneIndent;
                this.laneHeight = sizes.laneHeight;
                this.itemDistance = sizes.laneItemDistance;
                this.y = (this.count) * (this.laneHeight + sizes.laneDistance);
            }
            BranchLane.prototype.draw = function (paper, listener) {
                this.rect = paper.rect(0, this.y, this.laneWidth, this.laneHeight);
                this.rect.attr("fill", this.color.bg);
                this.rect.attr("stroke", this.color.bg);
                this.drawBranchLine(paper, listener);
                this.laneWidth += this.drawBranchTitle(paper);
            };
            BranchLane.prototype.resize = function (newWidth) {
                this.rect.attr("width", newWidth);
            };
            BranchLane.prototype.drawBranchTitle = function (paper) {
                var textHeight = this.sizes.laneLabelHeight;
                var y = this.y + (this.laneHeight / 2);
                var t = paper.text(this.laneWidth + this.indent, y, this.repositoryItem.name);
                t.attr({ "font-size": textHeight, "font-family": "RobotoDraft" });
                t.attr("fill", this.color.text);
                return t.node['textLength'].baseVal.value;
            };
            BranchLane.prototype.drawBranchLine = function (paper, listener) {
                var height = this.sizes.elementHeight / 4;
                var y = this.y + (this.laneHeight / 2);
                var command = "M" + this.left + "," + y + "L" + this.right + "," + y;
                var path = paper.path(command);
                path.attr("stroke", this.color.fg);
                path.attr("stroke-width", height);
                if (listener) {
                    this.registerMouseListener(path, listener);
                }
            };
            Object.defineProperty(BranchLane.prototype, "laneNumber", {
                get: function () {
                    return this.count;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Get coordinates of the given column number.
             *
             * The x-coordinate points points to the left most position of the new item
             * The y-coordinate points to the middle of the new item (i.e. middle of the lane)
             */
            BranchLane.prototype.getPosition = function (col) {
                var x = this.indent + ((this.itemDistance + 1 + this.sizes.laneHeight) * col);
                var y = this.y + (this.laneHeight / 2);
                // Update leftmost and rightmost positions and overall width
                this.left = Math.min(this.left, x);
                this.right = Math.max(this.right, x);
                this.laneWidth = Math.max(this.laneWidth, x + this.sizes.elementHeight + this.indent);
                return new Position(x, y);
            };
            return BranchLane;
        })(RepositoryElement);
        /** A Connection between to branch elements */
        var Connection = (function (_super) {
            __extends(Connection, _super);
            function Connection(branchItemConnection, sizes, from, to) {
                _super.call(this, branchItemConnection, sizes);
                this.from = from;
                this.to = to;
                this.lineHeight = sizes.elementHeight / 4;
            }
            Connection.prototype.draw = function (paper, listener) {
                var command = "M" + this.from.position.x + "," + this.from.position.y + "L" + this.to.position.x + "," + this.to.position.y;
                var path = paper.path(command);
                var color;
                if (this.repositoryItem.connectionType == 'branchStart') {
                    color = this.to.bgColor;
                }
                else {
                    color = (this.from.branchLane.laneNumber > this.to.branchLane.laneNumber ? this.from.bgColor : this.to.bgColor);
                }
                path.attr("stroke", color);
                path.attr("stroke-width", this.lineHeight);
                this.registerMouseListener(path, listener);
            };
            return Connection;
        })(RepositoryElement);
        var RepositoryElementSelectedEventType;
        (function (RepositoryElementSelectedEventType) {
            RepositoryElementSelectedEventType[RepositoryElementSelectedEventType["Over"] = 0] = "Over";
            RepositoryElementSelectedEventType[RepositoryElementSelectedEventType["Out"] = 1] = "Out";
            RepositoryElementSelectedEventType[RepositoryElementSelectedEventType["Clicked"] = 2] = "Clicked";
        })(RepositoryElementSelectedEventType || (RepositoryElementSelectedEventType = {}));
        // TODO use dedicated methods instead of eventType enum ?
        (function (RepositoryItemSelectedEventType) {
            /** The Item has been selected (clicked with the mouse) */
            RepositoryItemSelectedEventType[RepositoryItemSelectedEventType["ItemSelected"] = 0] = "ItemSelected";
            /** The mouse is over the item */
            RepositoryItemSelectedEventType[RepositoryItemSelectedEventType["ItemOver"] = 1] = "ItemOver";
            /** Mouse has left the item */
            RepositoryItemSelectedEventType[RepositoryItemSelectedEventType["ItemOut"] = 2] = "ItemOut";
        })(ui.RepositoryItemSelectedEventType || (ui.RepositoryItemSelectedEventType = {}));
        var RepositoryItemSelectedEventType = ui.RepositoryItemSelectedEventType;
        var GitHistoryGraph = (function () {
            function GitHistoryGraph(panelElementId, repositoryMatrix) {
                this.panelElementId = panelElementId;
                this.repositoryMatrix = repositoryMatrix;
                this.uiConfig = new UiConfig();
                this.sizes = DEFAULT_SIZES;
                this.lanes = [];
                this.commits = {};
                this.connections = [];
            }
            GitHistoryGraph.prototype.setup = function () {
                this.repositoryMatrix.accept({
                    visitBranchRow: function (br) {
                        var branchLane = new BranchLane(this.lanes.length, this.sizes, br.logicalBranch, this.uiConfig.colorForLane(this.lanes.length));
                        this.lanes.push(branchLane);
                    },
                    visitBranchCol: function (c) {
                        var branchLane = this.lanes[this.lanes.length - 1];
                        var branchElement;
                        var position = branchLane.getPosition(c.position);
                        if (c.item.type === 'BranchStart') {
                            branchElement = new BranchStartElement(branchLane, this.sizes, c, position, branchLane.color.fg);
                        }
                        else if (c.item.type === 'CombinedCommit') {
                            branchElement = new CombinedCommitElement(branchLane, this.sizes, c, position, branchLane.color);
                        }
                        else {
                            branchElement = new SingleCommitElement(branchLane, this.sizes, c, position, branchLane.color.fg);
                        }
                        this.commits[c.item.id] = branchElement;
                    },
                    visitConnection: function (c) {
                        var commit1 = this.commits[c.fromBranchItemId];
                        var commit2 = this.commits[c.toBranchItemId];
                        var connection = new Connection(c, this.sizes, commit1, commit2);
                        this.connections.push(connection);
                    }
                }, this);
            };
            GitHistoryGraph.prototype.setColorScheme = function (colorScheme) {
                this.uiConfig.setColorScheme(colorScheme);
            };
            GitHistoryGraph.prototype.setSizes = function (sizes) {
                this.sizes = sizes;
            };
            GitHistoryGraph.prototype.draw = function (raphaelCustomizer) {
                var _this = this;
                // STEP 1: GET WIDEST LANE
                // --------------------------------------------------------------------------------------------------
                var width = 0;
                var height = 0;
                this.lanes.forEach(function (branchLane) {
                    width = Math.max(width, branchLane.laneWidth);
                    height = Math.max(height, (branchLane.y + branchLane.laneHeight));
                });
                // STEP 1: MAKE ALL LINES SAME WIDTH
                // --------------------------------------------------------------------------------------------------
                this.lanes.forEach(function (branchLane) {
                    branchLane.laneWidth = width;
                });
                // STEP 2: CREATE THE 'PAPER'
                // --------------------------------------------------------------------------------------------------
                // TODO calculate width
                var paper = Raphael(this.panelElementId, width + 200, height);
                if (raphaelCustomizer) {
                    raphaelCustomizer(paper);
                }
                // STEP 2-A: ADD ALL LANES
                var newWidth = 0;
                this.lanes.forEach(function (branchLane) {
                    branchLane.draw(paper, _this);
                    if (branchLane.laneWidth > newWidth) {
                        newWidth = branchLane.laneWidth;
                    }
                });
                // STEP 2-B: RE-AJUST THE LANE WIDTHS
                this.lanes.forEach(function (branchLane) {
                    branchLane.resize(newWidth);
                });
                // STEP 3: DRAW CONNECTIONS
                // --------------------------------------------------------------------------------------------------
                this.connections.forEach(function (connection) {
                    connection.draw(paper, _this);
                });
                for (var key in this.commits) {
                    var commit = this.commits[key];
                    commit.draw(paper, this);
                }
            };
            GitHistoryGraph.prototype.setRepositoryItemSelectedEventListener = function (listener) {
                this.repositoryItemSelectedEventListener = listener;
            };
            GitHistoryGraph.prototype.repositoryElementSelected = function (event) {
                if (!this.repositoryItemSelectedEventListener) {
                    return;
                }
                var repositoryItemSelectedEventType;
                switch (event.eventType) {
                    case 2 /* Clicked */:
                        repositoryItemSelectedEventType = 0 /* ItemSelected */;
                        break;
                    case 1 /* Out */:
                        repositoryItemSelectedEventType = 2 /* ItemOut */;
                        break;
                    case 0 /* Over */:
                        repositoryItemSelectedEventType = 1 /* ItemOver */;
                        break;
                }
                this.repositoryItemSelectedEventListener({
                    branchItem: event.repositoryElement.repositoryItem,
                    eventType: repositoryItemSelectedEventType
                });
            };
            return GitHistoryGraph;
        })();
        ui.GitHistoryGraph = GitHistoryGraph;
    })(ui = gitvisualizer.ui || (gitvisualizer.ui = {}));
})(gitvisualizer || (gitvisualizer = {}));
//# sourceMappingURL=GitHistoryGraph.js.map