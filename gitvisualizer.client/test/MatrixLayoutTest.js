/*******************************************************************************
 * Copyright (c) 2014 Nils Hartmann (http://nilshartmann.net).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nils Hartmann (nils@nilshartmann.net) - initial API and implementation
 ******************************************************************************/
'use strict';

describe("List", function () {
	it('can transform items', function () {
		var list = new gitvisualizer.matrix.List();
		list.add('eins');
		list.add('zwei');
		list.add('drei');
		list.transform(function (oldValue) {
			return oldValue.toUpperCase();
		});

		expect(list.size()).toBe(3);
		expect(list.get(0)).toBe('EINS');
		expect(list.get(1)).toBe('ZWEI');
		expect(list.get(2)).toBe('DREI');
	});
	it('can iterate backwards', function() {
		var list = new gitvisualizer.matrix.List();
		var result = [];
		list.add('eins');
		list.add('zwei');
		list.add('drei');
		list.reverseForEach(function(c) {
			result.push(c);
		});
		expect(result.length).toBe(3);
		expect(result[0]).toBe('drei');
		expect(result[1]).toBe('zwei');
		expect(result[2]).toBe('eins');

	})
});

describe("RowAndCol", function () {
	var Row, Col;

	beforeEach(function () {
		Row = gitvisualizer.matrix.Row;
		Col = gitvisualizer.matrix.Cell;
	});

	it("A column can be moved right", function () {
		var row = new Row(0);
		var eins = new Col(null, "eins", row, 0);
		var zwei = new Col(eins, "zwei", row, 2);
		var drei = new Col(zwei, "drei", row, 4);
		row.add(eins);
		row.add(zwei);
		row.add(drei);

		zwei.moveTo(4);
		expect(eins.position).toBe(0);
		// Zwei wird auf Position 4 verschoben...
		expect(zwei.position).toBe(4);
		// drei wird um zwei Positionen verschoben (jetzt also 6)
		expect(drei.position).toBe(6);
	});


});

describe("MatrixLayout", function () {
	var matrix;

	function readFixture(name) {
		var data = getJSONFixture('fixtures/' + name + '.json');
		expect(data).not.toBeFalsy();
		return data;
	}

	function buildMatrix(fixtureName) {
		var model = readFixture(fixtureName);

		var builder = new matrix.MatrixBuilder(model);
		var theMatrix = builder.build();
		expect(theMatrix).not.toBeFalsy();
		expect(theMatrix.rows).not.toBeFalsy();
		return theMatrix;
	}

	var colToString = function (col) {
		return col.position + ":" + col.item.id;
	};

	var expectMatrixWithSize = function (theMatrix, expectedColumns) {
		expect(theMatrix.rows.length).toBe(expectedColumns.length);
		for (var i = 0; i < expectedColumns.length; i++) {
			expect(theMatrix.rows[i].size()).toBe(expectedColumns[i]);
		}
	};

	beforeEach(function () {
		if (window.TEST_FIXTURES_PATH) {
			// from testRunner.html
			jasmine.getJSONFixtures().fixturesPath = TEST_FIXTURES_PATH;
		} else {
			// via karma
			jasmine.getJSONFixtures().fixturesPath = 'base/test';
		}

		matrix = gitvisualizer.matrix;
	});

	it("moves elements as far as possible to the right", function () {
		var theMatrix = buildMatrix('three-branches-with-long-distance');
		expectMatrixWithSize(theMatrix, [3, 2, 8]);

		expect(theMatrix.mapRow(0, colToString)).toEqual(['0:A', '1:B', '2:C']);
		expect(theMatrix.mapRow(1, colToString)).toEqual(['0:D', '7:E']);
		expect(theMatrix.mapRow(2, colToString)).toEqual(['0:M1', '1:M2', '2:M3', '3:M4', '4:M5', '5:M6', '6:M7', '7:M8']);
	});

	it("createsASingleLineForASingleBranch", function () {
		var theMatrix = buildMatrix('one-branch');
		expect(theMatrix.rows.length).toBe(1);
		var rowOne = theMatrix.rows[0];
		expect(rowOne.rowNumber()).toBe(0);
		expect(rowOne.size()).toBe(3);
	});
	it("createsARowForEachIndependentBranch", function () {
		var theMatrix = buildMatrix('three-branches');
		expectMatrixWithSize(theMatrix, [3, 2, 4]);
	});

	it("correctlyWorksWithConnectionsPointingToSameTarget", function () {
		var theMatrix = buildMatrix('two-connections-with-same-target');
		expectMatrixWithSize(theMatrix, [3, 4]);
		expect(theMatrix.mapRow(0, colToString)).toEqual(['0:R1', '3:R2', '4:R3']);
		expect(theMatrix.mapRow(1, colToString)).toEqual(['0:M1', '1:M2', '2:M3', '3:M4']);
	});
	it("correctlyWorksWithConnectionsFromSameSource", function () {
		var theMatrix = buildMatrix('two-connections-with-same-source');
		expectMatrixWithSize(theMatrix, [3, 4]);
		expect(theMatrix.mapRow(1, colToString)).toEqual(['0:M1', '1:M2', '2:M3', '3:M4']);
		expect(theMatrix.mapRow(0, colToString)).toEqual(['0:R1', '2:R2', '3:R3']);
	});
	it("canWorkWithLargeGraphs", function () {
		var theMatrix = buildMatrix('gf-master-release-develop-feature-large');
		expectMatrixWithSize(theMatrix, [8, 2, 11, 2, 3, 4]);
		expect(theMatrix.mapRow(0, colToString)).toEqual(['0:M1', '2:M2', '6:M3', '7:M4', '8:M5', '9:M6', '10:M7', '11:M8']);
		expect(theMatrix.mapRow(1, colToString)).toEqual(['8:R1', '9:R2']);
		expect(theMatrix.mapRow(2, colToString)).toEqual(['0:D1', '1:D2', '2:D3', '4:D4', '5:D5', '6:D6', '7:D7', '8:D8', '9:D9', '10:D10', '11:D11']);
		expect(theMatrix.mapRow(3, colToString)).toEqual(['2:F1A', '3:F1B']);
		expect(theMatrix.mapRow(4, colToString)).toEqual(['3:F2A', '4:F2B', '5:F2C']);
		expect(theMatrix.mapRow(5, colToString)).toEqual(['3:F3A', '4:F3B', '7:F3C', '8:F3D']);
	});
	it('createsIndentForConnection', function () {
		var theMatrix = buildMatrix('two-branches-one-connection');
		expect(theMatrix.rows.length).toBe(2);
		theMatrix.mapRow(0, colToString)
		expect(theMatrix.mapRow(0, colToString)).toEqual(['2:R1']);
	});
	it('visitsAllElements', function () {
		var theMatrix = buildMatrix('two-branches-one-connection');
		var branchRowsVisited = [];
		var branchColsVisited = [];
		var connectionsVisited = [];
		theMatrix.accept({
			visitBranchRow:  function (br) {
				branchRowsVisited.push(br.logicalBranch.name);
			},
			visitBranchCol:  function (c) {
				branchColsVisited.push(c.item.id);
			},
			visitConnection: function (c) {
				connectionsVisited.push(c.fromBranchItemId + '->' + c.toBranchItemId);
			}
		});
		expect(branchRowsVisited).toEqual(['release', 'master']);
		expect(branchColsVisited).toEqual(['R1', 'M1', 'M2', 'M3']);
		expect(connectionsVisited).toEqual(['M2->R1']);
	});
	it('bindThisToContextObject', function () {
		var theMatrix = buildMatrix('two-branches-one-connection');

		var context = {
			key:               'value',
			rowVisited:        false,
			colVisited:        false,
			connectionVisited: false
		};

		theMatrix.accept({
			visitBranchRow:  function (br) {
				expect(this).toBe(context);
				this.rowVisited = true;
			},
			visitBranchCol:  function (c) {
				expect(this).toBe(context);
				this.colVisited = true;
			},
			visitConnection: function (c) {
				expect(this).toBe(context);
				this.connectionVisited = true;
			}
		}, context);

		expect(context.rowVisited).toBe(true);
		expect(context.colVisited).toBe(true);
		expect(context.connectionVisited).toBe(true);
	});

	describe("colorpicker", function () {

		function colorpicker(j) {
			var MAX = 3;
			//if (j<MAX) {
			//	return j;
			//}

			return j % MAX;
		}

		it("should pick a color", function () {
			expect(colorpicker(0)).toBe(0);
			expect(colorpicker(1)).toBe(1);
			expect(colorpicker(2)).toBe(2);
			expect(colorpicker(3)).toBe(0);
			expect(colorpicker(4)).toBe(1);
			expect(colorpicker(5)).toBe(2);
			expect(colorpicker(6)).toBe(0);

		});
	})

});